const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
///tables //TODO pobierac modele z innego miejsca
const OrderAddresses = require('./../../orders/models/order_addresses');
const UserAddresses = require('./../../users/models/user_addresses');

/** @see Dictionaries*/
/**
  * Address type
  * @typedef {Object} module:Dictionaries.Address
  * @property {char} id Symbol of address (pk)
  * @property {string} Name Name of address
  * @property {string=} Comment Description of address
  */
const DAddresses = database.define('DAddresses', {

  id:{
    type: Sequelize.STRING(1)
    ,primaryKey: true
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Name:{
    type: Sequelize.STRING(32)
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Comment:{
    type: Sequelize.STRING(256) 
  }
});

///static class methods
DAddresses.get = async function(id){  
  var response = await DAddresses.findById(id)
    .then((dAddress) => {
      return dAddress;
    })
    .catch(err => {throw err})

    return response
  }

DAddresses.list = async function(){
    return await this.findAll({order: Sequelize.col('Id')})
      .then(dAddresses => {return dAddresses})
      .catch(err => {throw err})
  }

///constraints
DAddresses.hasMany(OrderAddresses, {foreignKey: 'DAddressesId', targetKey: 'id'})
DAddresses.hasMany(UserAddresses, {foreignKey: 'DAddressesId', targetKey: 'id'})

///creating dictionaries (if not exiests)
DAddresses.sync();


module.exports = DAddresses