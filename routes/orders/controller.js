const Sequelize     = require('sequelize');
const ipaddr        = require('ipaddr.js');
const dateFormat    = require('dateformat');

const dbErrHandler  = require('../../services/diana_db').errHandler;
const Przelewy24    = require('../../services/przelewy24');
const paypal        = require('../../services/paypal');
const emailer       = require('../../services/email');

const Model         = require('./model.js');
const available     = require('./../parts/model.js').Part.available;

const Ordering      =require('./ordering')


const P24_TRUST_IPS = Przelewy24.P24_TRUST_IPS


/** Load order and append to req. */
async function load(req, res, next, id) {
  req.id = id
  req.order = await Model.Orders.get(id)
  next();
}

/** Load order and append to req. */
async function load_full(req, res, next, id) {
  req.id = id
  req.order = await Model.Orders.order(id)
  next();
}

/** append filter to req qr. */
async function load_by_user(req, res, next, id) {
    req.body.where = {UsersId: id};
    next();
}

function get(req, res) {
  if(req.order)
    res.status(200).json(req.order);
  else
    res.status(404).send('No such order with id: ' + req.id)
}

function list(req, res, next) {
  let query = {
    where: {}
    };

  req.qr

  if(Number.isInteger(parseInt(req.query.limit)))
    query.limit = parseInt(req.query.limit);
  if(Number.isInteger(parseInt(req.query.offset))) //checking if limit exiests and is number
    query.offset = parseInt(req.query.offset);

  if(req.body.order)
    query.order = req.body.order;

  if(req.body.where)
    query.where =req.body.where;

  Model.Orders.orders(query)
    .then(orders => res.json(orders))
    .catch(err => next(err));
}


function change_status(req, res, next){
  req.order.setStatus(req.query.status)
    .then(order => res.json(order))
    .catch(err => next(err));
}


async function pay(req, res, next){
  if(!req.order)
    next({message:'Zamówienie o numerze ' + req.id + ' nie istnieje.'});
  else{
    if(req.order.DStatusesId == 'O')
      next({message:'Zamówienie o numerze ' + req.id + ' zostało już opłacone.'});
    else{
      orderMail(req.order); //send order confirmation email    
      try{
        var payment = await req.order.payment();
      }catch(e){
        payment = '';
      }
      if(payment === 'Przelewy24') //sprawdzanie platnosci
        createPaymentPrzelewy24(req, res, next);
      else if(payment === 'PayPal')
        createPaymentPayPal(req, res, next);      
      else if(payment === 'Przelew')
        createPayment(req, res, next);
      else
        next({message:'Nie można zrealizować zamówienia, skonsultuj się z administratorem serwisu.'});
    } 
  }
}

async function orderMail(order){
  try{
    let emailAddress = await order.getEmail();
    let price = await order.price().fullPrice.toFixed(2) + 'zł';
    let mail  = await emailer.EmailTemplates.newOrderEmail(emailAddress,order.Symbol, price, order.Date);
    //sending email
    emailer.sendEmail(mail);
  }catch(e){
    console.log('Couldnt send confirmation email to order id: ' + order.id + ', symbol: ' + order.symbol + ' with error [' + e + ']');
  }
}

function createPayment(req, res, next){
  res.status(200).json({url: 'http://sklep.mamauto.pl/'});
}


async function createPaymentPrzelewy24(req, res, next) {
    const P24 = new Przelewy24();
    let order;
    let email;
    try{
      order = await Model.Orders.order(req.id);
      email = order.getEmail();
    }catch (e){
      res.status(500).send({message: e.message});
    }

    let price = order.price();
    let products = order.products();

    // Set obligatory data
    P24.setSessionId(req.id)
    P24.setAmount(price.fullPrice*100)
    P24.setCurrency('PLN')
    P24.setDescription('Oplata za zamowienie  ' + order.Symbol)
    P24.setEmail(email)
    P24.setShipping(price.shippingPrice*100);
    P24.setCountry('PL')
    P24.setUrlStatus('http://145.239.90.21:3000/api/orders/pay/verify/p24')
    P24.setUrlReturn('http://sklep.mamauto.pl')

    // add products to paymany
    for(let i =0; i< products.length; i++)
      P24.addProduct(products[i]);

    // Register order
    try {
        const token = await P24.register()
        const url = P24.getPayByLinkUrl(token)

        res.status(200).json({url:url});
    } catch (e) {
        res.status(500).send({message: e.message});
    }
}


async function callbackP24(req, res, next){
    var ip = req.headers['x-real-ip'] || req.connection.remoteAddress;
    ip = ipaddr.parse(ip).toString();
    if (P24_TRUST_IPS.indexOf(ip) === -1) {
        return next(new Error('Unauthorized IP address'))
    }

    const { p24_session_id, p24_amount, p24_currency, p24_order_id, p24_sign } = req.body;
    const P24 = new Przelewy24();

    P24.setSessionId(p24_session_id)
    P24.setAmount(p24_amount)
    P24.setCurrency(p24_currency)
    P24.setOrderId(p24_order_id)

    try {
        await P24.verify(p24_sign)
        order = await Model.Orders.get(p24_session_id)
        if(!order)
          throw res.status(500).send('Zły numer zamówienia');
          order.setStatus('O');
        return res.send('OK');
    } catch (e) {
        res.status(500).send(e.message);
    }
}



async function createPaymentPayPal(req, res, next){
  if(!req.order)
    next('Brak zamówienia');

  try{
    email = req.order.getEmail();
  }catch (e){
    next(e);
  }
  try{
    var create_payment_json = await paypal.createPaymentJson(req.order);
  }
  catch(e){
    next(e)
  }

  paypal.createPay(create_payment_json)
    .then(resp => res.status(200).json({url:resp}))
    .catch(e => next(e));
}


async function callbackPayPal(req, res, next){
  paypal.execPay(req.query.paymentId, req.query.PayerID)
    .then(resp => {
      if(!req.order)
        throw {status: 'Zły numer zamówienia'}
      req.order.setStatus('O');
      res.redirect('http://sklep.mamauto.pl')
    })
    .catch(e => next(e));
}



module.exports = { load, load_full, load_by_user, get, list, change_status, pay, callbackP24, Ordering, callbackPayPal }