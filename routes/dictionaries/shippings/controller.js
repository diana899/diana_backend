const Shippings = require('./model');
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler = require('../../../services/diana_db').errHandler;



function load(req, res, next, id) {
	Shippings.get(id)
		.then(shippings => {
			req.id = id;
			req.shippings = shippings; 
			next();
		})
		.catch(e => next(e));
}


function get(req, res) {
	if(req.shippings)
		res.status(200).json(req.shippings);
	else
		res.status(404).send('No shippings with id: ' + req.id + ' in dictionairy');
}


function create(req, res, next) {
	const shippings = new Shippings(req.body);
	shippings.save()
		.then(savedShipping => res.json(savedShipping))
		.catch(err => next(dbErrHandler(err)));
}


function update(req, res, next) {
	let shippings = req.shippings;
	shippings.update(req.body)
		.then(savedShipping => {res.json(savedShipping)})
		.catch(err => next(dbErrHandler(err)));   
}


function list(req, res, next) {
	Shippings.list()
		.then(shippings => res.json(shippings))
		.catch(err => next(dbErrHandler(err)));
}


function remove(req, res, next) {
	const shippings = req.shippings;
	shippings.destroy()
		.then(deletedShipping => res.json(deletedShipping))
		.catch(e => next(e));    
}



module.exports = { load, get, create, update, list, remove };