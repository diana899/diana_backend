const Sequelize = require('sequelize');
const database = require('../../services/diana_db').database //tmp database!!

//tables
const Complaints = require('./models/complaints');

const ComplaintMessages = require('./models/complaint_messages');

//audit table
const AudComplaints = require('./models/aud_complaints');

//dictionaries
const DComplaintStatuses = require('./../dictionaries/complaint_statuses/model');

//constraints
//Users.hasMany(Orders, {as: 'foto', foreignKey: 'id_czesci', targetKey: 'id_czesci'}) //przeniesc do Users
//Orders.belongsTo(Users, {foreignKey: 'id_czesci', targetKey: 'id_czesci'})

Complaints.hasMany(ComplaintMessages, {foreignKey: 'ComplaintsId', targetKey: 'id'})
ComplaintMessages.belongsTo(Complaints, {foreignKey: 'ComplaintsId', targetKey: 'id'})

Complaints.hasMany(AudComplaints, {foreignKey: 'ComplaintsId', targetKey: 'id'})
AudComplaints.belongsTo(Complaints, {foreignKey: 'ComplaintsId', targetKey: 'id'})

//dictionaries constraints
Complaints.belongsTo(DComplaintStatuses, {foreignKey: 'DComplaintStatusesId', targetKey: 'id'})
AudComplaints.belongsTo(DComplaintStatuses, {foreignKey: 'DComplaintStatusesId', targetKey: 'id'})

//creating tables (if not exiests)
Complaints.sync({force:false, alter:true});

ComplaintMessages.sync();
AudComplaints.sync();


//building complex queries
Complaints.fullList = async function(params){
  let query = params;
  query.distinct=true;
  if(!query.order)
    query.order = [
  ['Date', 'DESC']
  ,['AudComplaints','Date', 'DESC']
  ];
  query.include = [
    {
      model: ComplaintMessages
      ,required: false
      ,attributes: {exclude: ['id','ComplaintsId']}
    }
    ,{
      model: AudComplaints
      ,required: false
      ,attributes: {exclude: ['id','ComplaintsId']}
    }
  ];

  return await Complaints.findAndCountAll(query)
    .then(complaints => {return complaints})
    .catch(err => {throw err})
  }

Complaints.fullGet = async function(id){
  let query = {};
  query.where = {};
  query.where.id = id;
  query.order = [['AudComplaints','Date', 'DESC']]
  query.include = [
    {
      model: ComplaintMessages
      ,required: false
      ,attributes: {exclude: ['id','ComplaintsId']}
    }
    ,{
      model: AudComplaints
      ,required: false
      ,attributes: {exclude: ['id','ComplaintsId']}
    }
  ];

  return await Complaints.findOne(query)
    .then(ccmplaints => {return ccmplaints})
    .catch(err => {throw err})
  }


Complaints.prototype.setStatus = async function(newStatus){
  let oldStatus = this.DComplaintStatusesId;
  if(oldStatus!==newStatus){
      let oldDate = this.Date;
    if(this.ModDate) oldDate = this.ModDate;
    this.DComplaintStatusesId = newStatus;
    this.ModDate = new Date();
    try{
      let audComplaints = new AudComplaints({
        ComplaintsId          : this.id
        ,DComplaintStatusesId : oldStatus
        ,Date                 : oldDate
        });
      var ac = await audComplaints.save();
    }catch(e){
      throw e;
    }

    return await this.save() //save new status, and return result
      .then(savedComplaint => {return savedComplaint})
      .catch(err => {
        ac.destroy();
        throw err}); 
  }
  else
    throw {status:500, message: 'Zmiana na ten sam status'};
}

module.exports = { Complaints, ComplaintMessages, AudComplaints }