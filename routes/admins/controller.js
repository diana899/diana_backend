const Admin = require('./model').Admins;
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler = require('../../services/diana_db').errHandler;
const bcrypt = require('bcrypt');

var getToken = require('./../../services/authentication').getToken;

const saltRounds = 10;

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
	Admin.admin(id)
		.then(admin => {
			req.id = id;
			req.admin = admin; 
			next();
		})
		.catch(e => next(e));
}


/**
 * Get admin
 * @returns {Admin}
 */
function get(req, res) {
	if(req.admin){
		delete req.admin.Password;
		res.status(200).json(req.admin);
	}
	else
		res.status(404).send('No such admin with id: ' + req.id);
}

/**
 * Create new user
 * @property {string} req.body - The user.
 * @returns {Users} 
  */
function create(req, res, next) {
	const admin = new Admin({
		Login:          req.body.Login
		,Password:      req.body.Password
		,AdminRolesId:	req.body.AdminRolesId
	});

	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(req.body.Password, salt, function(err, hash) {
			if(!err){
				admin.Password = hash;
				admin.save()
					.then(savedAdmin => res.json(savedAdmin))
					.catch(err => next(err)); //    TODO: przenisesc obsluge bledow validacji do innej funkcji
			}
		});
	});
}

/**
 * Update existing user
 * @property {string} req.body.Nickname - The Nickname of user.
 * @property {string} req.body.Email    - The Email of user.
 * @property {string} req.body.Name     - The Name of user.
 * @property {string} req.body.LastName - The LastName of user.
 * @returns {user}
 */
function update(req, res, next) {
	let admin = req.admin;    //get user from load method
	//premove uneditable data
	if(req.body.Password)       delete(req.body.Password);
	admin.update(req.body)
		.then(savedAdmin => res.status(200).json(savedAdmin))
		.catch(err => next(err));   
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {user[]}
 */
function list(req, res, next) {
	Admin.admins({})
		.then(admins => res.json(admins))
		.catch(err => next(err));
}

/**
 * Delete user.
 * @returns {user}
 */
function remove(req, res, next) {
	const admin = req.admin;
	admin.destroy()
		.then(deletedAdmin => res.json(deletedAdmin))
		.catch(e => next(e));    
}


async function login(req, res, next) {
	let err = {status : 404
		,message : 'Wrong login or password'};

	Admin.login(req.body.Login)
		.then(admin => {
			if(admin){
				bcrypt.compare(req.body.Password, admin.Password).then(resp => {
					if(resp===true){
						admin.dataValues.token = getToken({Admin: admin});
						delete(admin.dataValues.Password);
						res.status(200).json(admin);
					}
					else  
						next(err);
				})
					.catch(e => next(e)); 
			}
			else  
				next(err);    
		})
		.catch(err => next(dbErrHandler(err)));
}

async function change_pass(req, res, next) {
	let err = {status : 404
		,message : 'Wrong password'};

	if(req.admin){
		bcrypt.compare(req.body.PasswordOld, req.admin.Password).then(resp => {
			if(resp===true){
				req.admin.Password = req.body.PasswordNew;
				tmp_change_pass(req, res, next);
			}
			else  
				next(err);
		})
			.catch(err => next(err)); 
	}
	else  
		next(err);
}


function tmp_change_pass(req, res, next){
	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(req.admin.Password, salt, function(err, hash) {
			if(!err){
				req.admin.Password = hash;
				req.admin.save()
					.then(savedAdmin => res.json(savedAdmin))
					.catch(err => next(dbErrHandler(err))); //    TODO: przenisesc obsluge bledow validacji do innej funkcji
			}
		});
	});
}




module.exports = { load, get, create, update, list, remove, login, change_pass};