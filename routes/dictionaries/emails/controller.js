const Email = require('./model');
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler = require('../../../services/diana_db').errHandler;



function load(req, res, next, id) {
	Email.get(id)
		.then(email => {
			req.id = id;
			req.email = email; 
			next();
		})
		.catch(e => next(e));
}


function get(req, res) {
	if(req.email)
		res.email(200).json(req.email);
	else
		res.email(404).send('No email with id: ' + req.id + ' in dictionairy');
}


function create(req, res, next) {
	const email = new Email(req.body);
	email.save()
		.then(savedEmail => res.json(savedEmail))
		.catch(err => next(dbErrHandler(err)));
}


function update(req, res, next) {
	let email = req.email;
	email.update(req.body)
		.then(savedEmail => {res.json(savedEmail)})
		.catch(err => next(dbErrHandler(err)));   
}


function list(req, res, next) {
	Email.list()
		.then(emails => res.json(emails))
		.catch(err => next(dbErrHandler(err)));
}


function remove(req, res, next) {
	const email = req.email;
	email.destroy()
		.then(deletedEmail => res.json(deletedEmail))
		.catch(e => next(e));    
}



module.exports = { load, get, create, update, list, remove};