var passport 	= require("passport");
var passportJWT = require("passport-jwt");
var jwt 		= require('jsonwebtoken');

var conf 		= require('./../configs/authentication');

const isToken = (process.env.TOKEN === 'true');

var JwtStrategy = passportJWT.Strategy;

//Authorization
//'Bearer' + ' ' + token
var strategy = new JwtStrategy(conf.jwtOptions, function(jwt_payload, next) {
  if(!isToken)
    next(null, {}); //gdy w .env wylaczono zabezpieczenie tokenem

  var user = null;
  // check authentication
  if(jwt_payload.UserId)
    user = {UserId: jwt_payload.UserId};
  else if(jwt_payload.AdminId)
    user = {AdminId: jwt_payload.AdminId, role: jwt_payload.role };
  
  if(!user)
    next(null, false);

  next(null, user);
});


/// PRIVATE functions
function checkACL(role, usersRoles){
  //if no role given
  if(!role) 
    return true;
  else if(role && !usersRoles)
    return false;
  //check roles
  else if(usersRoles.includes(role))
    return true;
  else
    return false;
}

function payloadAdmin(Admin){
  if(!Admin)
    throw 'no Admins object passed id'
  let admin = {};
  let roles = [];
  let Premissions = Admin.AdminRole.AdminRolePremissions;


  for(let i=0; i<Premissions.length;i++)
    roles.push(Premissions[i].Symbol);


  admin.AdminId = Admin.id
  admin.role = roles;

  return admin;
}


/// PUBLIC functions
function getToken({User , Admin }){
	if(Admin)
		var payload = payloadAdmin(Admin);

	if(User)
		var payload = {UserId: User.id};

	if(!Admin && !User)
		throw 'no Users or Admins id'
	return jwt.sign(payload, conf.secretOrKey);//, {expiresIn: conf.expTime});
}


function requireAuthACL(canUser, role){
  return(function authCustom(req, res, next) {
    passport.authenticate('jwt', { session: false }, function(err, user, info) {
      if (!isToken) return next();
      if (err) { return next(err); }
      if (!user) { 
        return next({status: 401, message: 'Brak uprawnień.'}); 
      } 
        if(user.UserId){
          req.userId = user.UserId;
          if(req.params.userId)
            if(user.UserId != req.params.userId)
              return next({status: 401, message: 'Niewystarczające uprawnienia.'});      
        }
        //check acl
        if(checkACL(role, user.role) || (canUser && user.UserId))
          return next();
        else
          return next({status: 401, message: 'Niewystarczające uprawnienia.'});

    })(req, res, next);
  })
}
                                                                       
const requireAuth = passport.authenticate('jwt', { session: false });


module.exports = { strategy, secretOrKey: conf.secretOrKey, getToken, requireAuth, requireAuthACL }
