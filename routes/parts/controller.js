const Sequelize = require('sequelize');

const Model = require('./model.js')
const Part = require('./model.js').Part

const Part_name = require('./model.js').Part_name
const Part_group = require('./model.js').Part_group

const Part_car_make = require('./model.js').Part_car_make
const Part_car_model = require('./model.js').Part_car_model

const search_engine = require('../../services/search_engine')

/**
 * Load part and append to req.
 */
async function load_part(req, res, next, id) {
  req.id = id
  req.part = await Part.part(id)
  next();
}


/**
 * Get part
 * @returns {Part}
 */
function get_part(req, res) {
  if(req.part)
    res.status(200).json(req.part);
  else
    res.status(404).send('No such part with id: ' + req.id)
}


/**
 * Book part with id
 * @returns {part}
 */
function book(req, res, next) { //  TODO: przeniesc do modelu: instance method
  let part = req.part;
  //if part exists, proceed
  if(part){
    if(part.rezerwacja === false){ //if its not activated
      part.rezerwacja = true //set to active
      part.save() //save changes to database
        .then(savedPart => res.status(200).json(savedPart)) //return activated part
        .catch(err => next(err));
    }
    else
      res.status(404).send('Part already booked');
  }
  else
    res.status(404).send('Wrong part id');
}


/**
 */
function parts_parts(req, res, next) {
  parts(req, res, next, Part)
}

/**
 */
function parts_groups(req, res, next) {
  parts(req, res, next, Part_group)
}

/**
 */
function parts_names(req, res, next) {
  parts(req, res, next, Part_name)
}

/**
 */
function parts_makes(req, res, next) {
  parts(req, res, next, Part_car_make)
}

/**
 */
function parts_models(req, res, next) {
  parts(req, res, next, Part_car_model)
}

function parts(req, res, next, model) {
  let qr = {};
  
  if(Number.isInteger(parseInt(req.query.limit)))
    qr.limit = parseInt(req.query.limit);
  if(Number.isInteger(parseInt(req.query.offset))) //checking if limit exiests and is number
    qr.offset = parseInt(req.query.offset);

  if(req.query.random == true)
    qr.order = [Sequelize.fn('RAND')];  // TODO: dodac rout na czesci polecane

  model.parts(qr)//{ limit: parseInt(limit), offset: parseInt(offset) }) //ORM didnt want to parse int by itself
    .then(parts => res.json(parts))
    .catch(err => next(err));
}


/**
 * Get part list.
 * @property {number} req.query.skip - Number of parts to be skipped.
 * @property {number} req.query.limit - Limit number of parts to be returned.
 * @returns {part[]}

function list_parts(req, res, next) {
  const { offset = 0, limit = 50 } = req.query; //get parameters from url, with default values
  Part.list({ limit: parseInt(limit), offset: parseInt(offset) }) //ORM didnt want to parse int by itself
    .then(parts => res.json(parts))
    .catch(err => next(err));
}
 */

/**
 */
function list_parts(req, res, next) {
  list(req, res, next, Part)
}

/**
 */
function list_names(req, res, next) {
  list(req, res, next, Part_name)
}

/**
 */
function list_groups(req, res, next) {
  list(req, res, next, Part_group)
}

/**
 */
function list_makes(req, res, next) {
  list(req, res, next, Part_car_make)
}

/**
 */
function list_models(req, res, next) {
  list(req, res, next, Part_car_model)
}


/**
 */
function list(req, res, next, model) {
  let qr = {};
  
  if(Number.isInteger(parseInt(req.query.limit)))
    qr.limit = parseInt(req.query.limit);
  if(Number.isInteger(parseInt(req.query.offset))) //checking if limit exiests and is number
    qr.offset = parseInt(req.query.offset);

  model.list(qr)
    .then(part_models => res.json(part_models))
    .catch(err => next(err));
}



function parts_query(req, res, next){
  let qr = {};
  
  if(Number.isInteger(parseInt(req.query.limit)))
    qr.limit = parseInt(req.query.limit); 
  if(Number.isInteger(parseInt(req.query.offset))) //checking if limit exiests and is number
    qr.offset = parseInt(req.query.offset);

  search_engine.parts_query(Model, req.body, qr)
    .then(parts => res.json(parts))
    .catch(err => next(err));
}



//var lists = {list_parts, list_names, list_groups, list_models, list_makes}
module.exports = { parts_query, load_part, get_part, parts_parts, book, list_parts, list_names, list_groups, list_models, list_makes, parts_groups, parts_names, parts_makes, parts_models }