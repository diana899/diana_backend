const express = require('express');
const controller = require('./controller.js');

const router = express.Router();

/** @see Dictionaries */

/** @namespace Dictionaries.api/dictionaries/statuses/
 */
router.route('/')
	/** Get list of statuses from dictionary
	 * @function
	 * @memberof Dictionaries.api/dictionaries/statuses/
	 * @name GET /api/dictionaries/statuses/
	 * @returns {Status[]} list of [Status]{@link module:Dictionaries.Status}
	 */
	.get(controller.list)
	/** 
	 * Create new status
	 * @function
	 * @memberof Dictionaries.api/dictionaries/statuses/
	 * @name POST /api/dictionaries/statuses/
	 * @param {Status} [Status]{@link module:Dictionaries.Status} BODY
	 * @returns {Status|Error} [Status]{@link module:Dictionaries.Status}
	*/
	.post(controller.create);

router.route('/:synonymeId')
	/** 
	* Get status by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/statuses/
	* @name GET /api/dictionaries/statuses/
	* @param {number} statusId PARAM
	* @returns {Status|Error} [Status]{@link module:Dictionaries.Status}
	*/
	.get(controller.get)
	/** 
	* Update status by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/statuses/
	* @name PUT /api/dictionaries/statuses/
	* @param {number} statusId PARAM
	* @param {Status} Status - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {Status|Error} [Status]{@link module:Dictionaries.Status}
	*/
	.put(controller.update)
	/** 
	* Delete status by symbol
	* @memberof Dictionaries.api/dictionaries/statuses/
	* @name DELETE /api/dictionaries/statuses/
	* @function
	* @param {number} statusId PARAM
	* @returns {Status|Error} [Status]{@link module:Dictionaries.Status}
	*/
	.delete(controller.remove);

///** Load status when API route parameter is hit */
router.param('synonymeId', controller.load);




module.exports = router;