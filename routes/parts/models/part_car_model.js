const Sequelize = require('sequelize');
const database = require('../../../services/ares_db').database //tmp database!!


//part_car_model schema
const Part_car_model = database.define('modele_samochodow', {

  id_model:{
    type: Sequelize.INTEGER
    ,primaryKey: true
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,id_marki:{
    type: Sequelize.INTEGER
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,nazwa_marki:{
    type: Sequelize.STRING(16) 
    ,allowNull: false
  }

});

//static class methods

/**
 * Get part_car_model by id
 * @property {number} id - Parts id
 * @returns {part_car_model}
  */
Part_car_model.get = async function(id){  
  var response = await part_car_model.findById(id)
    .then((part_car_model) => {
      return part_car_model;
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part_car_model list within range.
 * @property {number} skip - Number of part_car_models to be skipped.
 * @property {number} limit - Limit number of part_car_models to be returned.
 * @returns {part_car_model[]}
  */
Part_car_model.list = async function(range){
    range.order = Sequelize.col('id_model')//adding order by id to findAll params
    return await this.findAll(range)
      .then(part_car_models => {return part_car_models})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//Part_car_model.sync();

module.exports = { Part_car_model }