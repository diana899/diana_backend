const Model         = require('./model.js');
const Part          = require('./../parts/model.js').Part;
const dateFormat    = require('dateformat');
const Sequelize     = require('sequelize');

module.exports = class Ordering{
  static create(req, res, next) {
    Ordering.check_order(req.body)
      .then(orderFull => Ordering.createOrder(req, res, next, orderFull))
      .catch(err => {next(err)});
  }

  static async check_order(orderFull){
    if((!(orderFull.OrderElements.length>0)) || (!orderFull.OrderElements)) 
      throw {Status: 500, message: 'Brak elementów zamowienia'};
    if(!(orderFull.OrderAddresses.length>0) || (!orderFull.OrderAddresses)) 
      throw {Status: 500, message: 'Brak adresów'};

    //delete unsafe fields
    delete orderFull.DStatusesId;
    delete orderFull.id;
    delete orderFull.AdminsId;
    delete orderFull.Date;
    delete orderFull.ModDate;
   
    try{
      orderFull.NumberInMonth = await Model.Orders.getNextNumber();
    }
    catch(e){
      throw e;
    }

    orderFull.Symbol = orderFull.NumberInMonth + '/SKL/' + dateFormat(new Date(), 'mm/yyyy');

    const order = new Model.Orders(orderFull);

    let orderAddresses = [];
    let orderElements = [];
    try{
      for(let i=0; i<orderFull.OrderAddresses.length; i++)
        orderAddresses.push(new Model.OrderAddresses(orderFull.OrderAddresses[i]));
      orderElements =orderFull.OrderElements;
    }
    catch(err)
    {
      throw 'Zamówienie jest niekompletne';
    }

    return {order, orderAddresses, orderElements}
  }

  static async createOrder(req, res, next, orderFull){
    return Model.Orders.sequelize.transaction(async (t) => { 
      let savedOrder = await orderFull.order.save();    
      return await Ordering.add_order_props(orderFull, savedOrder);
    }).then((result) => {
        res.json(result);
      }).catch((err) => {
        next(err);
      });
  }

  static async add_order_props(orderFull, savedOrder){ 
    await Ordering.create_address(orderFull.orderAddresses,savedOrder.id)
      .then(OrderAddresses => {
        savedOrder.dataValues.OrderAddresses = OrderAddresses;     
      })
      .catch(err => {throw err});

    await Ordering.create_elements(orderFull.orderElements,savedOrder.id)
        .then(OrderElements =>{
          savedOrder.dataValues.OrderElements = OrderElements;
        })
        .catch(err => {throw err});
    return savedOrder;
  }

  static async create_address(orderAddresses, ordersId){
    let addresses = [];
    for(let i=0; i<orderAddresses.length;i++){
      orderAddresses[i].OrdersId = ordersId;
      await orderAddresses[i].save()
        .then(savedAddress => addresses.push(savedAddress))
        .catch(err => {throw err});
    }
    return addresses;
  }

  static async create_elements(partsId, ordersId){
    let elements  = [];
    let parts     = [];
    try{
      parts = await Part.get_available_list(partsId); //pobieranie listy dostepnych czesci, wraz ze sprawdzeniem ilosci
      for(let i=0;i<parts.length;i++){
        let orderElement = new Model.OrderElements(parts[i].toElement());
        orderElement.OrdersId = ordersId;
        await orderElement.save()
          .then(savedElement => elements.push(savedElement))         
      }
    }catch(err){   
      throw err;
    }
    return elements;
  }
}