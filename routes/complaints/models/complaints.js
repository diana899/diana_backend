const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
const dateFormat = require('dateformat');

/** @see Complaints */
/**
 * Complain type
 * @typedef {Object} module:Complaints.Complain
 * @property {string} Symbol unique AUTO
 * @property {number} id_czesci FK to amb_czesci
 * @property {number} UsersId FK to Users
 * @property {char!} DComplaintStatusesId FK to DComplaintStatuses
 * @property {date} Date creation date
 * @property {date} ModDate last modification date
 * @property {string} ModAdminsLogin
 * @property {AudComplaints[]} AudComplaints [AudComplaints]{@link module:Complaints.AudComplaints}
 * @property {ComplaintMessages[]} ComplaintMessages [ComplaintMessages]{@link module:Complaints.ComplaintMessages}
 */
const Complaints = database.define('Complaints', {

  Symbol:{
    type: Sequelize.STRING(20)
  }

  ,id_czesci:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,UsersId:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,DComplaintStatusesId:{
    type: Sequelize.STRING(1)
    ,allowNull: false
    ,defaultValue: 'N'
  }
 
  ,Date:{
    type: Sequelize.DATE
    ,defaultValue: Sequelize.NOW
    ,allowNull: false
    ,validate: {
      isDate: true
    }
  }  

  ,Text:{
    type: Sequelize.TEXT
  }

  ,ModDate:{
    type: Sequelize.DATE
    ,defaultValue: null
    ,validate: {
      isDate: true
    }
  } 

  ,ModAdminsLogin:{
    type: Sequelize.STRING(64)
  }

});

//static class methods

/**
 * Get part by id
 * @property {number} id - Parts id
 * @returns {part}
  */
Complaints.get = async function(id){  
  return  await Complaints.findById(id)
    .then((orders) => {
      return orders; // eslint-disable-line no-param-reassign
    })
    .catch(err => {throw err})
  }

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
Complaints.list = async function(range){
    range.order = Sequelize.col('Id')//adding order by id to findAll params
    return await this.findAll(range)
      .then(complaint => {return complaint})
      .catch(err => {throw err})
  }


//instance methods


module.exports = Complaints