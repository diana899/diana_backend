const express = require('express');
const controller = require('./controller.js')

var requireAuthACL = require('./../../services/authentication').requireAuthACL;

const router = express.Router();

/** Express router providing orders related routes
 * @module Orders
 */




///  COMMON actions
/** @namespace Orders.api/orders/ */
router.route('/')
	/** Get list of orders
	 * @function
	 * @memberof Orders.api/orders/
	 * @name GET /api/orders/
	 * @param {char[]} status QUERY, order status filter, Example: /api/orders/?status=["N","D"]
	 * @param {string[][]} order QUERY, ordering rules, Exapmle: /api/orders/?order=[["Date","DESC"]]
	 * @returns {Order[]} list of [Order]{@link module:Orders.Order}
	 */
	.get(requireAuthACL(canUser = false, 'orders_list')
		,controller.list)
	/**
	 * Create new order
	 * @function
	 * @memberof Orders.api/orders/
	 * @name POST /api/orders/
	 * @param {Order} [Order]{@link module:Orders.Order} BODY
	 * @returns {Order|Error} [Order]{@link @link module:Orders.Order}
	*/
	.post(requireAuthACL(canUser = true, 'orders_create')
		,controller.Ordering.create);

router.route('/:orderId')
	/** 
	* Get order by id
	* @function
	* @memberof Orders.api/orders/
	* @name GET /api/orders/
	* @param {number} orderId PARAM
	* @returns {Order|Error} [Order]{@link module:Orders.Order}
	*/
	.get(requireAuthACL(canUser = false, 'orders_get')
		,controller.get)


///  USERs actions
/** @namespace Orders.api/orders/user/ */
router.route('/user/:usersId')
	/** 
	* Get order by users id
	* @function
	* @memberof Orders.api/orders/user/
	* @name GET /api/orders/user
	* @param {number} userId PARAM
	* @returns {Order[]|Error} [Order]{@link module:Orders.Order}
	*/
	.get(requireAuthACL(canUser = true, 'orders_user_list')
		,controller.list)

/** @namespace Orders.api/orders/pay/ */
router.route('/pay/:orderId')
	/** 
	* Pay for order
	* @function
	* @memberof Orders.api/orders/pay/
	* @name PUT /api/orders/pay/
	* @param {number} orderId PARAM
	* @returns {url|Error} url
	*/
	.put(requireAuthACL(canUser = true)
		,controller.pay)

/** @namespace Orders.api/orders/address/ 
 *  @ignore
 */
router.route('/address/:orderId') 
	/** 
	* @ignore
	* Change address in order
	* @function
	* @memberof Orders.api/orders/address/
	* @name PUT api/orders/address/
	* @param {number} orderId PARAM
	* @returns {Order[]|Error} [Order]{@link module:Orders.Order}
	*/
	.put(requireAuthACL(canUser = false)
		,function(req, res, next){res.status(500).send('not implemented')})


///  ADMINs actions
/** @namespace Orders.api/orders/status/ */
router.route('/status/:orderId')
	/**
	* Change orders status
	* @function
	* @memberof Orders.api/orders/status/
	* @name PUT /api/orders/status/
	* @param {number} orderId PARAM
	* @param {char} status QUERY, pk from DStatusesId
	* @returns {Order|Error} [Order]{@link module:Orders.Order}
	*/
	.put(requireAuthACL(canUser = false, 'orders_status')
		,controller.change_status)

/** @namespace Orders.api/orders/list/anal/ */
router.route('/list/anal/')
	/** Get list of orders
	 * @function
	 * @memberof  Orders.api/orders/list/anal/
	 * @name GET /api/orders/list/anal/
	 * @param {char[]} status QUERY, order status filter, Example: /api/orders/?status=["N","D"]
	 * @param {string[][]} order QUERY, ordering rules, Exapmle: /api/orders/?order=[["Date","DESC"]]
	 * @returns {Order[]} list of [Order]{@link module:Orders.Order}
	 */
	.post(requireAuthACL(canUser = false, 'user_list_anal')
		,controller.list)


router.route('/pay/pal/exec/:orderId')
	.get(controller.callbackPayPal);

router.route('/pay/verify/p24/')
	/** 
	* Rout for response from przelewy24.pl
	* @function
	* @memberof Orders.api/orders/pay/
	*/
	.post(controller.callbackP24)


/// PARAMETERS
///** Load order when API route parameter is hit */
router.param('orderId', controller.load_full);

///** Load orders by users id when API route parameter is hit */
router.param('usersId', controller.load_by_user);


module.exports = router;