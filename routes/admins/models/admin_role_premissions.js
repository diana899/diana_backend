const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database; //tmp database!!


/** @see Users */
/**
 * UserAddress type
 * @ignore
 * @typedef {Object} module:Users.UserAddress 
 * @property {string} DAddressesId
 * @property {string=} Email Validated
 * @property {string=} Name
 */
const AdminRolePremissions = database.define('AdminRolePremissions', {

	AdminRolesId: {
		type: Sequelize.INTEGER
		,allowNull: false
		,validate: {
			notEmpty: true
			,isNumeric: true
      		,isInt: true
		}
	}

	,Symbol: {
		type: Sequelize.STRING(26)
		,unique: true 
		,allowNull: false
		,validate: {
			notEmpty: true
		}
	}

	,Comment: {
		type: Sequelize.STRING(256)
	}

});

//static class methods

/**
 * Get part_car_model by id
 * @property {number} id - Parts id
 * @returns {part_car_model}
  */
AdminRolePremissions.get = async function(id){  
	return await AdminRolePremissions.findById(id)
		.then((adminRolePremissions) => {
			return adminRolePremissions;
		})
		.catch(err => {throw err;});
};

/**
 * Get part_car_model list within range.
 * @property {number} skip - Number of part_car_models to be skipped.
 * @property {number} limit - Limit number of part_car_models to be returned.
 * @returns {part_car_model[]}
  */
AdminRolePremissions.list = async function(range){
	range.order = Sequelize.col('id');//adding order by id to findAll params
	return await this.findAll(range)
		.then(adminRolePremission => {return adminRolePremission;})
		.catch(err => {throw err;});
};


//instance methods

//create or check if table exiest
//Part_car_model.sync();

module.exports = AdminRolePremissions;