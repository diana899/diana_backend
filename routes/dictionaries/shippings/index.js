const express = require('express');
const controller = require('./controller.js');

const router = express.Router();

/** 
 * @see Dictionaries
 */

/** @namespace Dictionaries.api/dictionaries/shippings/ */
router.route('/')
	/** Get list of shippings from dictionary
	 * @function
	 * @memberof Dictionaries.api/dictionaries/shippings/
	 * @name GET /api/dictionaries/shippings/
	 * @returns {Shipping[]} list of [Shipping]{@link module:Dictionaries.Shipping}
	 */
	.get(controller.list)
	/** 
	 * Create new shipping
	 * @function
	 * @memberof Dictionaries.api/dictionaries/shippings/
	 * @name POST /api/dictionaries/shippings/
	 * @param {Shipping} [Shipping]{@link module:Dictionaries.Shipping} BODY
	 * @returns {Shipping|Error} [Shipping]{@link module:Dictionaries.Shipping}
	*/
	.post(controller.create);

router.route('/:shippingId')
	/** 
	* Get shipping by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/shippings/
	* @name GET /api/dictionaries/shippings/
	* @param {number} statusId PARAM
	* @returns {Shipping|Error} [Shipping]{@link module:Dictionaries.Shipping}
	*/
	.get(controller.get)
	/** 
	* Update shipping by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/shippings/
	* @name PUT /api/dictionaries/shippings/
	* @param {number} statusId PARAM
	* @param {Shipping} Shipping - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {Shipping|Error} [Shipping]{@link module:Dictionaries.Shipping}
	*/
	.put(controller.update)
	/** 
	* Delete shipping by symbol
	* @memberof Dictionaries.api/dictionaries/shippings/
	* @name DELETE /api/dictionaries/shippings/
	* @function
	* @param {number} statusId PARAM
	* @returns {Shipping|Error} [Shipping]{@link module:Dictionaries.Shipping}
	*/
	.delete(controller.remove);

///** Load shipping when API route parameter is hit */
router.param('shippingId', controller.load);




module.exports = router;