const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!


/** @see Complaints */
/**
 * ComplaintMessages type
 * @typedef {Object} module:Complaints.ComplaintMessages 
 * @property {number} ComplaintsId AUTO
 * @property {string} AdminsLogin set null for users response
 * @property {date} Date AUTO
 * @property {string} Subject
 * @property {string} Text
 */
const ComplaintMessages = database.define('ComplaintMessages', {

  ComplaintsId:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,AdminsLogin:{
    type: Sequelize.STRING(64)
  }

  ,Date:{
    type: Sequelize.DATE
    ,defaultValue: Sequelize.NOW
    ,allowNull: false
    ,validate: {
      isDate: true
    }
  } 

  ,Subject:{
    type: Sequelize.STRING(64) 
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Text:{
    type: Sequelize.TEXT
  }

});

//static class methods

/**
 * Get part_car_model by id
 * @property {number} id - Parts id
 * @returns {part_car_model}
  */
ComplaintMessages.get = async function(id){  
  return  await ComplaintMessages.findById(id)
    .then((complaintMessage) => {
      return complaintMessage;
    })
    .catch(err => {throw err})
  }

/**
 * Get part_car_model list within range.
 * @property {number} skip - Number of part_car_models to be skipped.
 * @property {number} limit - Limit number of part_car_models to be returned.
 * @returns {part_car_model[]}
  */
ComplaintMessages.list = async function(range){
    range.order = Sequelize.col('Id')//adding order by id to findAll params
    return await this.findAll(range)
      .then(complaintMessages => {return complaintMessages})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//Part_car_model.sync();

module.exports = ComplaintMessages