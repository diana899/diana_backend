const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!


/** @see Orders */
/**
 * OrderElements type
 * @typedef {Object} module:Orders.OrderElements
 * @property {string=} OrdersId AUTO
 * @property {string} id_czesci FK to amb_czesci
 * @property {string} PartName
 * @property {string} PartComment
 * @property {number} PartPrice Encrypted
 */
const OrderElements = database.define('OrderElements', {

  OrdersId:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,id_czesci:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,comment: 'FK to czesci table in ares database'
    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,PartName:{
    type: Sequelize.STRING(64) 
  }

  ,PartComment:{
    type: Sequelize.STRING(8104) 
  }

  ,PartPrice:{
    type: Sequelize.DOUBLE
    ,validate: {
      isNumeric: true
    }
  }


});

//static class methods

/**
 * Get part_car_make by id
 * @property {number} id - Parts id
 * @returns {part_car_make}
  */
OrderElements.get = async function(id){  
  var response = await OrderElements.findById(id)
    .then((orderElements) => {
      return orderElements;
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part_car_make list within range.
 * @property {number} skip - Number of part_car_makes to be skipped.
 * @property {number} limit - Limit number of part_car_makes to be returned.
 * @returns {part_car_make[]}
  */
OrderElements.list = async function(range){
    range.order = Sequelize.col('Id')//adding order by id to findAll params
    return await this.findAll(range)
      .then(orderElements => {return orderElements})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//OrderElements.sync();

module.exports = OrderElements