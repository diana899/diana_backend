const Sequelize = require('sequelize');
const database = require('../../services/diana_db').database; //tmp database!!

//tables
const Admins = require('./models/admins');
const AdminRoles = require('./models/admin_roles');
const AdminRolePremissions = require('./models/admin_role_premissions');

//constraints
AdminRoles.hasMany(Admins, {foreignKey: 'AdminRolesId', targetKey: 'id'});
Admins.belongsTo(AdminRoles, {foreignKey: 'AdminRolesId', targetKey: 'id'});

AdminRoles.hasMany(AdminRolePremissions, {foreignKey: 'AdminRolesId', targetKey: 'id'});
AdminRolePremissions.belongsTo(AdminRoles, {foreignKey: 'AdminRolesId', targetKey: 'id'});


//creating tables (if not exiests)
Admins.sync();
AdminRoles.sync();
AdminRolePremissions.sync();


//building complex queries
Admins.admins = async function(params){
	let qr = params;

	qr.attributes = {exclude: ['Password', 'AdminRolesId']};
	qr.include = [
		{
			model: AdminRoles
			,required: false
			//,attributes: {exclude: ['id']}
		}
	];
	return await Admins.findAndCountAll(qr)
		.then(users => {return users;})
		.catch(err => {throw err;});
};

Admins.admin = async function(id){
	let qr = {};
	qr.where = {};
	qr.where.id = id;

	qr.include = [
		{
			model: AdminRoles
			,required: true
			,attributes: {exclude: ['id']}
			,include: [
				{
					model: AdminRolePremissions
					,required: true
					,attributes: {exclude: ['AdminRolesId']}
				}
			]
		}
	];

	return await Admins.findOne(qr)
		.then(admin => {return admin})
		.catch(err => {throw err;});
};


Admins.login = async function(login){ 
	let qr = {};
	qr.where = {};
	qr.where.Login = login;

	qr.include = [
		{
			model: AdminRoles
			,required: true
			//,attributes: {exclude: ['id']}
			,include: [
				{
					model: AdminRolePremissions
					,required: true
					//,attributes: {exclude: ['id']}
				}
			]
		}
	];

	return await Admins.findOne(qr)
		.then((admin) => {
			return admin;
		})
		.catch(err => {throw dbErrHandler(err);});
};


module.exports = { Admins, AdminRoles, AdminRolePremissions };