const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database;
const dbErrHandler = database.errHandler;


/** @see Admins */
/**
 * Admins type
 * @typedef {Object} module:Admins.Admin
 * @property {string} Login
 * @property {string} Password Encrypted
 * @property {number} AdminRolesId FK to [AdminRoles]{@link module:Admins.AdminRoles }
 */
const Admins = database.define('Admins', {

	Login: {
		type: Sequelize.STRING(64)
		,unique: true 
		,allowNull: false
		,validate: {
			notEmpty: true
		}
	}

	,Password: {
		type: Sequelize.STRING(60)
		,allowNull: false
		,validate: {
			notEmpty: true
		}
	}

	,AdminRolesId: {
		type: Sequelize.INTEGER
		,allowNull: false
		,validate: {
			notEmpty: true
			,isNumeric: true
      		,isInt: true
		}
	}

});


//static class methods

/**
 * @ignore
 * Get user by id
 * @property {number} id - Users id
 * @returns {user}
  */
Admins.get = async function(id){  
	return await Admins.findById(id)
		.then((admin) => {
			return admin;
		})
		.catch(err => {throw dbErrHandler(err);});
};



/**
 * @ignore
 * Get user list within range.
 * @property {number} skip - Number of users to be skipped.
 * @property {number} limit - Limit number of users to be returned.
 * @returns {user[]}
  */
Admins.list = async function(range){
	let order = Sequelize.col('id');//adding order by id to findAll params
	let params = {order : Sequelize.col('id')};
	return await this.findAll(params)
		.then(admins => {return admins;})
		.catch(err => {throw dbErrHandler(err);});
};



module.exports = Admins;