var md5 = require('md5');
var http = require('http');
var fs = require('fs');

//const wp = 'http://176.108.134.97:8088/'
const token = md5('nie_bede_wiecej_pisac_kodu_o_5_w_nocy');
const apiKey = 'c855b5d786f241877f415e236ea99000';
const validAuth = md5(token + '--##--' + apiKey);
//const url = 'sklep_mamauto/miniapi.php?auth=' + validAuth + '&token=' + token + '&magazyn=mamAUTO';

const delPartUrl = '&command=getdealscart&start=';


// var download = function(url, dest, cb) {
//   var file = fs.createWriteStream(dest);
//   var request = http.get(url, function(response) {
//     response.pipe(file);
//     file.on('finish', function() {
//       file.close(cb);  // close() is async, call cb after close completes.
//     });
//   }).on('error', function(err) { // Handle errors
//     fs.unlink(dest); // Delete the file async. (But we don't check the result)
//     if (cb) cb(err.message);
//   });
// };


// var deletePartAres = async (part_id) => {
//   let delPartUrl = delPartUrl + part_id;
//   let data = await request.get(wp + delPartUrl);
// }

//console.log(url);


async function buildOrderAresResponse(order){
  if(!order)
    throw 'no_order';

  let price     = order.price().fullPrice; // get full price of order
  let addressF  = order.getAddress('B'); // get billing address from order
  let addressM  = order.getAddress('M'); // get main address from order
  let payment   = await order.payment(); // get name of paymetn method
  let email     = order.getEmail();      // get contact email from order

  if(!addressM)
    throw 'no_address';
  if(!price)
    throw 'no_price';
  if(!payment)
    throw 'no_payment_method';
  if(!email)
    throw 'no_email';

  let wartosc_z_przesylka = price;
  let f_vat       = (order.IsInvoice?1:0);
  let message     = 'sklep.mamauto.pl';
  let adress_f    = addressF.Address;
  let postCode_f  = addressF.PostalCode;
  let city_f      = addressF.City;
  let fullName_f  = addressF.Name + ' ' + addressF.LastName;
  let phone_f     = addressF.PhoneNumber;
  let company_f   = addressF.CompanyName;
  let nip_f       = addressF.NIP;
  let adressType_f= '0';
  let adress      = addressM.Address;
  let postCode    = addressM.PostalCode;
  let city        = addressM.City;
  let fullName    = addressM.Name + ' ' + addressM.LastName;
  let phone       = addressM.PhoneNumber;
  let company     = addressM.CompanyName;
  let adressType  = '0';
  let platnosc    = payment;
  let wartosc_przesylki = order.ShippingPrice;
  let r_email     = email;

  let response = `${wartosc_z_przesylka};${f_vat};${message};${adress_f}${postCode_f};${city_f};${fullName_f};${phone_f};${company_f};${nip_f};${adressType_f};${adress};${postCode};${city};${fullName};${phone};${company};${adressType};${platnosc};${wartosc_przesylki};${r_email};`;
  response = response.replace(new RegExp('null', 'g'), ''); //raplace all nulls with ''
  return response;
}

function authAres(auth, token){ //TODO: zbadac co i jak z tego ze to?
  const validToken  = md5('nie_bede_wiecej_pisac_kodu_o_5_w_nocy');
  const apiKey    = 'c855b5d786f241877f415e236ea99000';
  const validAuth   = md5(validToken + '--##--' + apiKey);
  return (token === validToken && auth === validAuth);
}

async function formatDealItems(orders){
  if(!orders)
    throw 'no orders'
  if(orders.length<=0)
    throw 'no orders'

 let response = {
  status: 'OK'
  ,num: orders[0].id
  ,deals:{}
 }
 let counter = 0;

  for(let i=0; i<orders.length;i++){
    let order = orders[i];
    let orderAres = {
      nazwa: 'mam'
      ,cartid: order.id
    };
    if(order.OrderElements)
      if(order.OrderElements.length >0)
        for(let j=0; j<order.OrderElements.length; j++){
          let orderEl = orderAres;
          orderEl.kod = order.OrderElements[j].id_czesci;
          response.deals[counter++] = orderEl;
        }
  }

  return response;
}


module.exports = { authAres, buildOrderAresResponse, formatDealItems }