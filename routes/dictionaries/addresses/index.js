const express = require('express');
const controller = require('./controller.js');

const router = express.Router();

/** 
 * @see Dictionaries
 */

/** @namespace Dictionaries.api/dictionaries/addresses/ */
router.route('/')
	/** Get list of addresses from dictionary
	 * @function
	 * @memberof Dictionaries.api/dictionaries/addresses/
	 * @name GET /api/dictionaries/addresses/
	 * @returns {Address[]} list of [Address]{@link module:Dictionaries.Address}
	 */
	.get(controller.list)
	/** 
	 * Create new address
	 * @function
	 * @memberof Dictionaries.api/dictionaries/addresses/
	 * @name POST /api/dictionaries/addresses/
	 * @param {Address} Addresses - [Address]{@link module:Dictionaries.Address} BODY
	 * @returns {Address|Error} [Address]{@link module:Dictionaries.Address}
	*/
	.post(controller.create);

router.route('/:addressId')
	/** 
	* Get address by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/addresses/
	* @name GET /api/dictionaries/addresses/
	* @param {number} statusId PARAM
	* @returns {Address|Error} [Address]{@link module:Dictionaries.Address}
	*/
	.get(controller.get)
	/** 
	* Update address by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/addresses/
	* @name PUT /api/dictionaries/addresses/
	* @param {number} statusId PARAM
	* @param {Address} Address - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {Address|Error} [Address]{@link module:Dictionaries.Address}
	*/
	.put(controller.update)
	/** 
	* Delete address by symbol
	* @memberof Dictionaries.api/dictionaries/addresses/
	* @name DELETE /api/dictionaries/addresses/
	* @function
	* @param {number} statusId PARAM
	* @returns {Address|Error} [Address]{@link module:Dictionaries.Address}
	*/
	.delete(controller.remove);

///** Load address when API route parameter is hit */
router.param('addressId', controller.load);




module.exports = router;