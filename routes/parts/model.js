const Sequelize = require('sequelize');
const database  = require('../../services/ares_db').database //tmp database!!
const Op        = Sequelize.Op;

//tables
const Part = require('./models/part').Part;

const Part_foto = require('./models/part_foto').Part_foto;

const Part_name = require('./models/part_name').Part_name;
const Part_group = require('./models/part_group').Part_group;

const Part_car_make = require('./models/part_car_make').Part_car_make;
const Part_car_model = require('./models/part_car_model').Part_car_model;

const Allegro_group = require('./models/allegro_group');


//config
const link = 'http://ajax.ambit.pl/';



//constraints
  //parts table constraints
Part.hasMany(Part_foto, {as: 'foto', foreignKey: 'id_czesci', targetKey: 'id_czesci'})
Part_foto.belongsTo(Part, {foreignKey: 'id_czesci', targetKey: 'id_czesci'})

Part.belongsTo(Part_name, {foreignKey: 'id_nazwa', targetKey: 'id_nazwa'})
Part_name.hasMany(Part, {foreignKey: 'id_nazwa', targetKey: 'id_nazwa'})

Part.belongsTo(Part_group, {foreignKey: 'id_grupa', targetKey: 'id_grupa'})
Part_group.hasMany(Part, {foreignKey: 'id_grupa', targetKey: 'id_grupa'})

Part.belongsTo(Part_car_make, {foreignKey: 'id_marka', targetKey: 'id_marki'})
Part_car_make.hasMany(Part, {foreignKey: 'id_marka', targetKey: 'id_marki'})

Part.belongsTo(Part_car_make, {foreignKey: 'id_marka', targetKey: 'id_marki'})
Part_car_make.hasMany(Part, {foreignKey: 'id_marka', targetKey: 'id_marki'})

Part.belongsTo(Part_car_model, {foreignKey: 'id_model', targetKey: 'id_model'})
Part_car_model.hasMany(Part, {foreignKey: 'id_model', targetKey: 'id_model'})


Part_group.hasMany(Part_name, {foreignKey: 'id_grupa', targetKey: 'id_grupa'})
Part_name.belongsTo(Part_group, {foreignKey: 'id_grupa', targetKey: 'id_grupa'})

Part_car_model.belongsTo(Part_car_make, {foreignKey: 'id_marki', targetKey: 'id_marki'})
Part_car_make.hasMany(Part_car_model, {foreignKey: 'id_marki', targetKey: 'id_marki'})

Allegro_group.hasOne(Part, {foreignKey: 'id_nazwa', targetKey: 'item_ares'})
Part.hasOne(Allegro_group, {foreignKey: 'item_ares', targetKey: 'id_nazwa'})

//creating tables (if not exiests)
//Part.sync();

//Part_foto.sync();

//Part_group.sync();
//Part_name.sync();

//Part_car_make.sync();
//Part_car_model.sync();

//Allegro_group.sync();


const include = [
      {
        model: Part_name
        ,required: true
        ,attributes: ['nazwa_czesci']
      }
      ,{
        model: Part_group
        ,required: true
        ,attributes: ['id_grupa','nazwa_grupy']
      }
      ,{
        model: Part_car_model
        ,required: false
        ,attributes: [['nazwa_marki','name']]
      }
      ,{
        model: Part_car_make
        ,required: false
        ,attributes: [['nazwa_marki','name']]
      }
      ,{
        model: Part_foto
        ,required: false
        ,as: 'foto'
        ,attributes: ['foto_link']
      }
      ,{
        model: Allegro_group
        ,required: false
      }
    ];


//joining tables TODO: przeniesc skladanie query w inne miejsce (razem z search engine -> query builder)
Part.parts = async function(params){

  let qr = params;

  qr.attributes = ['id_czesci','rocznik_pas_od', 'rocznik_pas_do', 'rocznik', 'cena_nowej', 'cena_proponowana',  'komentarz_cz','przebieg','id_nazwa','id_grupa', 'id_marka','id_model' ];

  qr.include = include;

  //qr.distinct = true;

  return await Part.findAll(qr) //findAndCountAll
    .then(parts => {return parts})
    .catch(err => {throw err})
}

Part.part = async function(id){

  let qr = {where : {id_czesci : id}};

  qr.attributes = ['id_czesci','rocznik_pas_od', 'rocznik_pas_do', 'rocznik', 'cena_nowej', 'cena_proponowana',  'komentarz_cz','przebieg','id_nazwa','id_grupa', 'id_marka','id_model' ];

  qr.include = include;
  /*     [
        {
          model: Part_name
          ,required: true
          ,attributes: ['nazwa_czesci']
        }
        ,{
          model: Part_group
          ,required: true
          ,attributes: ['nazwa_grupy']
        }
        ,{
          model: Part_car_model
          ,attributes: ['name']
        }
        ,{
          model: Part_car_make
          ,attributes: ['name']
        }
        ,{
            model: Part_foto
            ,required: false
            ,as: 'foto'
            ,attributes: ['foto_link']
          }
      ];*/

  return await Part.findOne(qr)
    .then(parts => {return parts})
    .catch(err => {throw err})
}



Part_group.parts = async function(){
    return await Part_group.findAll({
      include: [{
          model: Part_name
          ,include: [{
            model: Part
              ,include: [{model: Part_car_model
              ,attributes: ['name']
            }
            ,{
              model: Part_car_make
              ,attributes: ['name']
            }]
          }]
        }
      ]}
    )
    .then(parts => {return parts})
    .catch(err => {throw err})
  }

Part_name.parts = async function(){
    return await Part_name.findAll({
      include: [
            {
            model: Part_group
            }
            ,{
            model: Part
              ,include: [{model: Part_car_model
              ,attributes: ['name']
            }
            ,{
              model: Part_car_make
              ,attributes: ['name']
          }]
        }
      ]}
    )
    .then(parts => {return parts})
    .catch(err => {throw err})
  }

Part_car_make.parts = async function(){
    return await Part_car_make.findAll({
      include: [
        {
          model: Part_car_model
          ,include: [{
            model: Part
              ,include: [
              {
                model: Part_group
              }
              ,{
                model: Part_name
              }
            ]
          }]
        }]
      }
    )
    .then(parts => {return parts})
    .catch(err => {throw err})
  }

Part_car_model.parts = async function(){
    return await Part_car_model.findAll({
      include: [
            {
            model: Part_car_make
            }
            ,{
            model: Part
              ,include: [{model: Part_group
            }
            ,{
              model: Part_name
          }]
        }
      ]}
    )
    .then(parts => {return parts})
    .catch(err => {throw err})
  }


/**
 * Return all available parts from list.
 * @property {partId} partId Parts id.
 * @returns {bool} returns true if available, false if booked  
 */
Part.get_available_list = async function(partsId){
  let where = {
    where: {
      id_czesci: {
        [Op.in]: partsId
      }
      ,rezerwacja : 0
    }
    ,include : [
      {
        model: Part_name
        ,required: true
        ,attributes: ['nazwa_czesci']
      }
    ]
  }
  return  await Part.findAll(where)
    .then((parts) => {
        if(!parts) throw {message:'Nie wszystkie czesci były dostępne'};
        else if(parts.length !== partsId.length) throw {message:'Nie wszystkie czesci były dostępne'};
        else return parts;
    })
    .catch(err => {throw err})
} 

Part.prototype.toElement = function(){
  let element = {
    OrdersId:     null
    ,id_czesci:   this.id_czesci
    ,PartName:    this.nazwy_czesci.nazwa_czesci
    ,PartComment: this.komentarz_cz
    ,PartPrice:   this.cena_proponowana
  }
  return element;
}


module.exports = { Part, Part_name, Part_group, Part_car_make, Part_car_model, Part_foto, Sequelize }