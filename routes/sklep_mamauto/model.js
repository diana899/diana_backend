const Sequelize = require('sequelize');
const database = require('../../services/diana_db').database; //tmp database!!

//tables
const Users = require('./models/users');
const UserAddresses = require('./models/user_addresses');

//dictionaries
const DAddresses = require('./../dictionaries/addresses/model');

//constraints
Users.hasMany(UserAddresses, {foreignKey: 'UsersId', targetKey: 'id'});
UserAddresses.belongsTo(Users, {foreignKey: 'UsersId', targetKey: 'id'});


//dictionaries constraints
UserAddresses.belongsTo(DAddresses, {foreignKey: 'DAddressesId', targetKey: 'id'});

//creating tables (if not exiests)
Users.sync();

UserAddresses.sync({force:false, alter:true});


//building complex queries
Users.users = async function(params){

	let qr = params;
	qr.distinct = true;
	
	if(!qr.order) 
		qr.order = [['id','ASC']];

	qr.attributes = {exclude: ['Password', 'ActivationKey']};

	qr.include = [
		{
			model: UserAddresses
			,required: false
			,attributes: {exclude: ['id', 'UsersId']}
		}
	];


	return await Users.findAndCountAll(qr)
		.then(users => {return users;})
		.catch(err => {throw err;});
};

UserAddresses.fullList = async function(params){
	let query = params;	
	query.distinct = true;
	query.attributes = {exclude: ['id']};
	if(!query.order) 
		query.order = [['id','ASC']];

	if(!query.where)
		query.where = {DAddressesId: 'M'}		
	else
		query.where.DAddressesId = 'M'



	query.include = [
		{
			model: Users
			,required: true
			,attributes: {exclude: ['id','Password', 'ActivationKey']}
		}
	];

	return await UserAddresses.findAndCountAll(query)
		.then(userAddresses => {return userAddresses;})
		.catch(err => {throw err;});
};

Users.user = async function(id){

	let qr = {};
	qr.where = {};

	qr.attributes = {exclude: ['ActivationKey']};

	qr.where.id = id;

	qr.include = [
		{
			model: UserAddresses
			,required: false
			,attributes: {exclude: ['id', 'UsersId']}
		}
	];

	return await Users.findOne(qr)
		.then(user => {return user;})
		.catch(err => {throw err;});
};

Users.update_adr = function(usr){
	return new Promise(function (resolve, reject){
		if(!(usr.UserAddresses > 0)) resolve();
		let adr = usr.UserAddresses;
		let savedAdr = [];
		for(let i=0; i< adr.length; i++){
			adr[i].UsersId = usr.id;
			let whr = {UsersId: adr[i].UsersId, DAddressesId: adr[i].DAddressesId};
			UserAddresses.findOrCreate({where: whr, defaults: adr[i].dataValues})
				.spread((userAdr, created) => {
					if(!created)
						userAdr.update(adr[i].dataValues) //TODO naprawa obslugi bledow
							.then(saved => savedAdr.push(saved))
							.catch(err => reject(err));
				})
				.catch(err => reject(err));
		}
		resolve(savedAdr)
	});
};


Users.prototype.updateAdress = function(adr){
	let id = this.id;
	return new Promise(function (resolve, reject){
		adr.UsersId = id;
		let whr = {UsersId: adr.UsersId, DAddressesId: adr.DAddressesId};
		UserAddresses.findOrCreate({where: whr, defaults: adr})
			.spread((userAdr, created) => {
				if(!created)
					userAdr.update(adr)
						.then(saved => resolve(saved))
						.catch(err => {
							reject(err)});
				else		
					resolve(userAdr);
			})
			.catch(err => reject(err));
	});
}

Users.prototype.updateAdresses = async function(adresses){
	if(!(adresses.length > 0)) throw 'Err';
	var savedAdr = [];
	for(let i=0; i< adresses.length; i++){
		let adress = await this.updateAdress(adresses[i]);
		savedAdr.push(adress.dataValues);
	}
	return savedAdr;
}

Users.prototype.delete_adr = function(type){
	let usr = this;
	return new Promise(function (resolve, reject){
		UserAddresses.findOne({where: {UsersId: usr.id, DAddressesId: type}})
			.then((userAdr) => {
				if(!userAdr)
					reject({status:'500', message:'Brak adresu typu ' + type})
					userAdr.destroy();
				resolve(userAdr);
			})
			.catch(err => reject(err));
	});
};


Users.prototype.getEmail = async function(){
  let userAddresses = await UserAddresses.findAll({where:{UsersId:this.id}});
	var email;

  if(userAddresses){
  	let len = userAddresses.length;
		if(len<1)
			throw {status:500, message: 'Brak adresu email w zamówieniu'};
		let type = {M: false, B: false};

		for(let i=0; i < len;i++){
		  if(userAddresses[i].Email){
		    if(!type.M && !type.B){
		      email = userAddresses[i].Email;
		    }
		    if(userAddresses[i].DAddressesId === 'M' && !type.B){
		      type.M = true;
		      email = userAddresses[i].Email;
		    }
		    if(userAddresses[i].DAddressesId === 'B'){
		      type.B = true;
		      email = userAddresses[i].Email;
		    }       
		  }
		}
	}
	else
		email = this.Email;

  if(email!==null)
    return email;
  else
    throw {status:500, message: 'Brak adresu email w zamówieniu'}; 
}


module.exports = { Users, UserAddresses };