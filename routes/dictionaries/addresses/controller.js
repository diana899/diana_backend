const Address = require('./model');
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler = require('../../../services/diana_db').errHandler;



function load(req, res, next, id) {
	Address.get(id)
		.then(address => {
			req.id = id;
			req.address = address; 
			next();
		})
		.catch(e => next(e));
}


function get(req, res) {
	if(req.address)
		res.status(200).json(req.address);
	else
		res.status(404).send('No address with id: ' + req.id + ' in dictionairy');
}


function create(req, res, next) {
	const address = new Address(req.body);
	address.save()
		.then(savedAddress => res.json(savedAddress))
		.catch(err => next(dbErrHandler(err)));
}


function update(req, res, next) {
	let address = req.address;
	address.update(req.body)
		.then(savedAddress => {res.json(savedAddress)})
		.catch(err => next(dbErrHandler(err)));   
}


function list(req, res, next) {
	Address.list()
		.then(addresses => res.json(addresses))
		.catch(err => next(dbErrHandler(err)));
}


function remove(req, res, next) {
	const address = req.address;
	address.destroy()
		.then(deletedAddress => res.json(deletedAddress))
		.catch(e => next(e));    
}



module.exports = { load, get, create, update, list, remove};