INSERT INTO AdminRoles (Symbol, Name, Comment)
Values 
	('ADMIN', 'Administrator Główny', 'Administrator Główny')
    ,('SPRZEDAWCA', 'Administrator Sprzedaży', 'Administrator Sprzedaży, posiada dostęp do przeglądu wpłat i zamówień, ma możliwość edycji danych klienta i zmiany statusu zamówienia')
;