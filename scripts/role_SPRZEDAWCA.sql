SET @id_role = (SELECT id FROM AdminRoles WHERE symbol = 'SPRZEDAWCA');
DELETE FROM AdminRolePremissions WHERE AdminRolesId = @id_role;
Insert into AdminRolePremissions (AdminRolesId, Symbol, Comment)
Values
	(@id_role , 'user_list',		'GET users/')
	,(@id_role , 'create_user',		'POST users/')
	,(@id_role , 'get_user',		'GET users/:usersId')
	,(@id_role , 'update_user',		'PUT users/:usersId')
	,(@id_role , 'delete_user',		'DELETE users/:usersId')
;