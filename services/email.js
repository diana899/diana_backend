const nodemailer = require('nodemailer');
const DEmails = require('./../routes/dictionaries/emails/model');

const host = process.env.EMAIL_HOST;
const user = process.env.EMAIL_USER;
const pass = process.env.EMAIL_PASS;

const EMAIL = (process.env.EMAIL !== 'false');


const transporter = nodemailer.createTransport({
  host: host,
  secure: true,
  auth: {
    user: user,
    pass: pass
  }
});



function sendEmail(email){
  if(EMAIL){
    if(!email.to)
      throw {status:500, message: 'Brak odbiorcy wiadomści email.'};
    var mailOptions = {
      from: user,
      to: email.to,
      subject: email.subject,
      html: email.html
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email to: '+  info.envelope.to + ' sent: ' + info.response);
      }
    });
  }
}





async function confirmEmial(userEmail, userActivationLink){
  return await DEmails.getBySymbol('REGISTER')
    .then(email => {
      let emailTemp = {};
      emailTemp.to = userEmail;
      emailTemp.subject = email.Subject;
      emailTemp.html = email.Text;
      emailTemp.html = emailTemp.html.replace('#!LINK!#', userActivationLink);
      return emailTemp;
    })
    .catch(e => {throw e});
}

async function newOrderEmail(userEmail,orderSymbol, orderPrice, orderDate){
  return await DEmails.getBySymbol('NEW_ORDER')
    .then(email => {
      let date = orderDate.toISOString().slice(0,10);
      let emailTemp = {};
      emailTemp.to = userEmail;
      emailTemp.subject = email.Subject;
      emailTemp.html = email.Text;
      emailTemp.html = emailTemp.html.replace('#!SYMBOL!#', orderSymbol);
      emailTemp.html = emailTemp.html.replace('#!CENA!#', orderPrice);
      emailTemp.html = emailTemp.html.replace('#!DATA!#', date);
      return emailTemp;
    })
    .catch(e => {throw e});
}

async function newComplaintMail(userEmail,complaintSymbol, complaintDate, id_czesci){
  return await DEmails.getBySymbol('NEW_COMPLAINT')
    .then(email => {
      let date = complaintDate.toISOString().slice(0,10);
      let emailTemp = {};
      emailTemp.to = userEmail;
      emailTemp.subject = email.Subject;
      emailTemp.html = email.Text;
      emailTemp.html = emailTemp.html.replace('#!SYMBOL!#', complaintSymbol);
      emailTemp.html = emailTemp.html.replace('#!DATA!#', date);
      emailTemp.html = emailTemp.html.replace('#!CZESC!#', id_czesci);
      return emailTemp;
    })
    .catch(e => {throw e});
}



var EmailTemplates = { confirmEmial, newOrderEmail, newComplaintMail };

module.exports = { sendEmail, EmailTemplates }