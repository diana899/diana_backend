const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database
//tables //TODO pobierac modele z innego miejsca
const Complaints = require('./../../complaints/models/complaints');

/** @see Dictionaries */

/**
 * ComplaintStatuses type
 * @typedef {Object} module:Dictionaries.ComplaintStatuses
 * @property {char} id Symbol of status (pk)
 * @property {string} Name Name of status
 * @property {string=} Comment Description of status
 */
const DComplaintStatuses = database.define('DComplaintStatuses', {

  id:{
    type: Sequelize.STRING(1)
    ,primaryKey: true
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Name:{
    type: Sequelize.STRING(32) 
    ,allowNull: false
    ,validate: {
    	notEmpty: true
    }
  }

  ,Comment:{
    type: Sequelize.STRING(256) 
  }
});

//static class methods
DComplaintStatuses.get = async function(id){  
  var response = await DComplaintStatuses.findById(id)
    .then((status) => {
      return status;
    })
    .catch(err => {throw err})

    return response
  }

DComplaintStatuses.list = async function(){
    return await this.findAll({order : Sequelize.col('id')})
      .then(dStatuses => {return dStatuses})
      .catch(err => {throw err})
  }

//constraints
DComplaintStatuses.hasMany(Complaints, {foreignKey: 'DComplaintStatusesId', targetKey: 'id'})

//creating dictionaries (if not exiests)
DComplaintStatuses.sync();


module.exports = DComplaintStatuses