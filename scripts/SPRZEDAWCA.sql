
DECLARE
	id_roli number;
BEGIN
	SELECT id INTO id_roli FROM AdminRoles WHERE symbol = 'SPRZEDAWCA';
	Insert into AdminRolePremissions (AdminRolesId, Symbol, Comment)
	Values(id_roli, 'user_list','users/' );
END;
/