const Sequelize = require('sequelize');
const database = require('../../../services/ares_db').database //tmp database!!


//part schema
const Part_name = database.define('nazwy_czesci', {

  id_nazwa:{
    type: Sequelize.INTEGER
    ,primaryKey: true
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,id_grupa:{
    type: Sequelize.INTEGER

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,nazwa_czesci:{
    type: Sequelize.STRING(64) 
  }

  ,nazwa_odpadu:{
    type: Sequelize.TEXT
  }

});

//static class methods

/**
 * Get part by id
 * @property {number} id - Parts id
 * @returns {part_name}
  */
Part_name.get = async function(id){  
  var response = await Part_name.findById(id)
    .then((part_name) => {
      return part_name;
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
Part_name.list = async function(range){  
    range.order = Sequelize.col('id_nazwa')//adding order by id to findAll params
    return await this.findAll(range)
      .then(part_names => {return part_names})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//Part_name.sync();

module.exports = { Part_name }