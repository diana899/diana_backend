var RateLimit     = require('express-rate-limit');

var userKey = function (req /*, res*/) {
  if(req.body.Email)
    return req.body.Email;
  else
    return req.ip;
};


var limiter = new RateLimit({
  windowMs: 2*60*1000, // 2 minutes
  max: 100, // limit each IP to 100 requests per windowMs
  delayMs: 0 // disable delaying - full speed until the max limit is reached
});


var createAccountLimiter = new RateLimit({
  windowMs: 60*60*1000, // 1 hour window
  delayAfter: 1, // begin slowing down responses after the first request
  delayMs: 3*1000, // slow down subsequent responses by 3 seconds per request
  max: 5, // start blocking after 5 requests
  message: "Too many accounts created from this IP, please try again after an hour"
});


var logginLimiter = new RateLimit({
  windowMs: 60*1000, // 1 minute window
  delayMs: 0.5*1000, //no delay
  max: 3, // start blocking after 3 requests
  message: "Too many login attempts",
  keyGenerator: userKey
});

module.exports = { limiter, createAccountLimiter, logginLimiter }