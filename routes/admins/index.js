const express = require('express');
const controller = require('./controller.js');
var requireAuthACL = require('./../../services/authentication').requireAuthACL;


const router = express.Router();


/** Express router providing admin related routes
 * @module Admins
 */

/** @namespace Admins.api/admins/ */
router.route('/')
	/** Get list of admins
	 * @function
	 * @memberof Admins.api/admins/
	 * @name GET /api/admins/
	 * @returns {Admin[]} list of [Admin]{@link module:Admins.Admin}
	 */
	.get(requireAuthACL(canUser = false, 'admins_list')
		,controller.list)
	/** 
	 * Create new admin
	 * @function
	 * @memberof Admins.api/admins/
	 * @name POST /api/admins/
	 * @param {Admin} Admin BODY
	 * @returns {Admin|Error} [Admin]{@link module:Admins.Admin}
	*/
	.post(requireAuthACL(canUser = false, 'admins_create')
		,controller.create);

router.route('/:adminId')
	/** 
	* Get admin by id
	* @function
	* @memberof Admins.api/admins/
	* @name GET /api/admins/
	* @param {number} userId PARAM
	* @returns {Admin|Error} [Admin]{@link module:Admins.Admin}
	*/
	.get(requireAuthACL(canUser = false, 'admins_get')
		,controller.get)
	/** 
	* Update admins data by id
	* @function
	* @memberof Admins.api/admins/
	* @name PUT /api/admins/
	* @param {number} userId PARAM
	* @param {Admin} Admin - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {Admin|Error} [Admin]{@link module:Admins.Admin}
	*/
	.put(requireAuthACL(canUser = false, 'admins_update')
		,controller.update)
	/** 
	* Delete admin by id
	* @memberof Admins.api/admins/
	* @name DELETE /api/admins/
	* @function
	* @param {number} userId PARAM
	* @returns {Admin|Error} [Admin]{@link module:Admins.Admin}
	*/
	.delete(requireAuthACL(canUser = false, 'admins_delete')
		,controller.remove);

///OTHER ROUTS
/** @namespace Admins.api/admins/login/ */
router.route('/login/')
	/** 
	* Login admin
	* @function
	* @memberof Admins.api/admins/login/
	* @name POST /api/admins/login/
	* @param {Object} Body BODY
	* @param {string} Body.Email
	* @param {string} Body.Password
	* @returns {Admin|Error} [Admin]{@link module:Admins.Admin}
	*/
	.post(requireAuthACL(canUser = false)
		,controller.login);

/** @namespace Admins.api/admins/password/ */
router.route('/password/:adminId')
	/** 
	* Change admins password
	* @function
	* @memberof Admins.api/admins/password/
	* @name PUT /api/admins/password/
	* @param {number} userId PARAM
	* @param {Object} Body BODY
	* @param {char} Body.PasswordOld 
	* @param {char} Body.PasswordNew
	* @returns {Admin|Error} [Admin]{@link module:Admins.Admin}
	*/
	.put(requireAuthACL(canUser = false)
		,controller.change_pass);


///** Load admin when API with activationKey route parameter is hit */
router.param('adminId', controller.load);




module.exports = router;