const Sequelize = require('sequelize');
const database = require('../../services/diana_db').database //tmp database!!

const Op = Sequelize.Op;

//tables
const Orders = require('./models/orders');

const OrderElements = require('./models/order_elements');
const OrderAddresses = require('./models/order_addresses');

//audit table
const AudOrders = require('./models/aud_orders');

//dictionaries
const DStatuses = require('./../dictionaries/statuses/model');
const DAddresses = require('./../dictionaries/addresses/model');
const DPayments = require('./../dictionaries/payments/model');
const DShippings = require('./../dictionaries/shippings/model');

//constraints
//Users.hasMany(Orders, {as: 'foto', foreignKey: 'id_czesci', targetKey: 'id_czesci'}) //przeniesc do Users
//Orders.belongsTo(Users, {foreignKey: 'id_czesci', targetKey: 'id_czesci'})

Orders.hasMany(OrderElements, {foreignKey: 'OrdersId', targetKey: 'id'})
OrderElements.belongsTo(Orders, {foreignKey: 'OrdersId', targetKey: 'id'})

Orders.hasMany(OrderAddresses, {foreignKey: 'OrdersId', targetKey: 'id'})
OrderAddresses.belongsTo(Orders, {foreignKey: 'OrdersId', targetKey: 'id'})

Orders.hasMany(AudOrders, {foreignKey: 'OrdersId', targetKey: 'id'})
AudOrders.belongsTo(Orders, {foreignKey: 'OrdersId', targetKey: 'id'})


//dictionaries constraints
Orders.belongsTo(DStatuses, {foreignKey: 'DStatusesId', targetKey: 'id'})
Orders.belongsTo(DPayments, {foreignKey: 'DPaymentsId', targetKey: 'id'})
Orders.belongsTo(DShippings, {foreignKey: 'DShippingsId', targetKey: 'id'})

OrderAddresses.belongsTo(DAddresses, {foreignKey: 'DAddressesId', targetKey: 'id'})


//creating tables (if not exiests)
Orders.sync();

OrderAddresses.sync();
OrderElements.sync();

AudOrders.sync();


var includeTemplate = [
  {
    model: OrderAddresses
    ,required: true
    ,attributes: {exclude: ['id', 'OrdersId']}
  }
  ,{
    model: OrderElements
    ,required: true
    ,attributes: {exclude: ['id', 'OrdersId']}
  }
  ,{
    model: AudOrders
    ,required: false
    ,attributes: {exclude: ['id', 'OrdersId']}
  }      
];


//building complex queries
Orders.orders = async function(params){
  let query = params;
  query.distinct=true;
  if(!query.order)
    query.order = [['Date', 'DESC']];
  query.order.push(['AudOrders','Date', 'DESC']); 
  //query.attributes = [];
  query.include = [
    {
      model: OrderAddresses
      ,required: true
      ,attributes: {exclude: ['id','OrdersId']}
    }
    ,{
      model: OrderElements
      ,required: true
      ,attributes: {exclude: ['id','OrdersId']}
    }
    ,{
      model: AudOrders
      ,required: false
      ,attributes: {exclude: ['id','OrdersId']}
    }
  ];

  return await Orders.findAndCountAll(query)
    .then(orders => {return orders})
    .catch(err => {throw err})
  }


Orders.order = async function(id){

  let qr = {};
  qr.where = {};

  qr.where.id = id;
  qr.order = [['AudOrders','Date', 'DESC']];
  //qr.attributes = [];

  qr.include = [
    {
      model: OrderAddresses
      ,required: true
      ,attributes: {exclude: ['id', 'OrdersId']}
    }
    ,{
      model: OrderElements
      ,required: true
      ,attributes: {exclude: ['id', 'OrdersId']}
    }
    ,{
      model: AudOrders
      ,required: false
      ,attributes: {exclude: ['id', 'OrdersId']}
    }      
  ];

  return await Orders.findOne(qr)
    .then(orders => {return orders})
    .catch(err => {throw err})
  }


Orders.prototype.price = function(){
  let fullPrice = this.ShippingPrice;
   for(let i=0; i<this.OrderElements.length;i++)
       fullPrice += this.OrderElements[i].PartPrice;
    return {fullPrice: fullPrice, shippingPrice: this.ShippingPrice};
  }


Orders.prototype.payment = async function(){
  try{
    let response = await DPayments.findById(this.DPaymentsId);
    if(!response.Name)
      throw ''
    return response.Name;
  }catch(e){    
    throw e;
  }
}


Orders.prototype.products = function(){
  let products = [];
  for(let i=0; i< this.OrderElements.length;i++){
    let oe = this.OrderElements[i];
    let product = {
      name: oe.PartName,
      description: oe.PartComment,
      quantity: 1,
      price: oe.PartPrice*100,
      number: oe.id_czesci
    }
    products.push(product);
  }
  return products;
}

Orders.prototype.getEmail = function(){
  let len = this.OrderAddresses.length;
  if(len<1)
    throw 'No email address in order';
  let type = {M: false, B: false};
  let email;

  for(let i=0; i < len;i++){
    if(this.OrderAddresses[i].Email){
      if(!type.M && !type.B){
        email = this.OrderAddresses[i].Email;
      }
      if(this.OrderAddresses[i].DAddressesId === 'M' && !type.B){
        type.M = true;
        email = this.OrderAddresses[i].Email;
      }
      if(this.OrderAddresses[i].DAddressesId === 'B'){
        type.B = true;
        email = this.OrderAddresses[i].Email;
      }       
    }
  }

  if(email!=null)
    return email;
  else
    throw 'No email address in order'; 
}


Orders.prototype.setStatus = async function(newStatus){
  let oldStatus = this.DStatusesId;
  let oldDate = this.Date;
  if(this.ModDate) oldDate = this.ModDate;
  
  this.DStatusesId = newStatus;
  this.ModDate = new Date();

  try{
    let audOrders = new AudOrders({
      OrdersId    : this.id
      ,DStatusesId : oldStatus
      ,Date        : oldDate
      });
    audOrders.save();
  }catch(e){
    throw e;
  }

  return await this.save() //save new status, and return result
    .then(savedOrder => {return savedOrder})
    .catch(err => {throw err}); 
}

Orders.prototype.getAddress = function(DAddressesId){
  let OrderAddresses = this.OrderAddresses;
  let address;
  for(let i = 0; i<OrderAddresses.length; i++){
    if(OrderAddresses[i].DAddressesId === DAddressesId)
      address = OrderAddresses[i];
  }
  if(!address)
    for(let i = 0; i<OrderAddresses.length; i++){
      if(OrderAddresses[i].DAddressesId === 'M')
        address = OrderAddresses[i];
  }
  return address;
}



//building complex queries
var ordersFrom = async function(id, limit){
  let query = {};
  //query.distinct=true;
  query.order = [['Id', 'ASC']]; 
  query.include = [
    {
      model: OrderAddresses
      ,required: true
      ,attributes: {exclude: ['id', 'OrdersId']}
    }
    ,{
      model: OrderElements
      ,required: true
      ,attributes: {exclude: ['id', 'OrdersId']}
    }
  ]
  if(!limit) limit = 100;
  query.limit = Number(limit);
  query.where = { Id: {[Op.gte]: id}};

  return await Orders.findAll(query)
    .then(orders => {return orders})
    .catch(err => {throw err})
  }


module.exports = { Orders, OrderAddresses, OrderElements, ordersFrom }