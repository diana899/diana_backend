const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
//tables //TODO pobierac modele z innego miejsca
const Orders = require('./../../orders/models/orders');

/** @see Dictionaries */

/**
 * Status type
 * @typedef {Object} module:Dictionaries.Status
 * @property {char} id Symbol of status (pk)
 * @property {string} Name Name of status
 * @property {string=} Comment Description of status
 */
const DStatuses = database.define('DStatuses', {

  id:{
    type: Sequelize.STRING(1)
    ,primaryKey: true
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Name:{
    type: Sequelize.STRING(32) 
    ,allowNull: false
    ,validate: {
    	notEmpty: true
    }
  }

  ,Comment:{
    type: Sequelize.STRING(256) 
  }
});

//static class methods
DStatuses.get = async function(id){  
  var response = await DStatuses.findById(id)
    .then((status) => {
      return status;
    })
    .catch(err => {throw err})

    return response
  }

DStatuses.list = async function(){
    
    return await this.findAll({order : Sequelize.col('id')})
      .then(dStatuses => {return dStatuses})
      .catch(err => {throw err})
  }

//constraints
DStatuses.hasMany(Orders, {foreignKey: 'DStatusesId', targetKey: 'id'})

//creating dictionaries (if not exiests)
DStatuses.sync();


module.exports = DStatuses