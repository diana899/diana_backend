const Status = require('./model');
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler = require('../../../services/diana_db').errHandler;



function load(req, res, next, id) {
	Status.get(id)
		.then(status => {
			req.id = id;
			req.status = status; 
			next();
		})
		.catch(e => next(e));
}


function get(req, res) {
	if(req.status)
		res.status(200).json(req.status);
	else
		res.status(404).send('No status with id: ' + req.id + ' in dictionairy');
}


function create(req, res, next) {
	const status = new Status(req.body);
	status.save()
		.then(savedStatus => res.json(savedStatus))
		.catch(err => next(dbErrHandler(err)));
}


function update(req, res, next) {
	let status = req.status;
	status.update(req.body)
		.then(savedStatus => {res.json(savedStatus)})
		.catch(err => next(dbErrHandler(err)));   
}


function list(req, res, next) {
	Status.list()
		.then(statuses => res.json(statuses))
		.catch(err => next(dbErrHandler(err)));
}


function remove(req, res, next) {
	const status = req.status;
	status.destroy()
		.then(deletedStatus => res.json(deletedStatus))
		.catch(e => next(e));    
}



module.exports = { load, get, create, update, list, remove};