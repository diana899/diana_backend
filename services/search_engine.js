var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'localhost:9200'
  //,log: 'trace'
});


function test_connection(req, res, next){
  client.ping({
    requestTimeout: 30000,
  }, function (error) {
    if (error) {
      res.status(500).send('elasticsearch is down!');
    } else {
      res.send('Elasticsearch is ON!');
    }
  });
}

/** @see Parts */
/**
 * User type
 * @typedef {Object} module:Users.User
 * @property {string} Email Validated
 * @property {string} Password Encrypted
 * @property {UserAddress[]} UserAdresses
 */
parts_query = async function(Model, body, query){

	let query_p = { "body": {"query": { "bool": {}}}};
	let filter = [];

	//let q = [];				TODO: usunac w nastepnej wersji (23.07.2018)
	// if(body.text){  
	// 	q.push({"match": { "komentarz_cz":  body.text}})
	// 	q.push({"match": { "nazwy_czesci.nazwa_czesci":  body.text}})
	// 	q.push({"match": { "marki_samochodow.name":  body.text}})
	// 	q.push({"match": { "modele_samochodow.name":  body.text}})
	// }

	//fitting year filter
	if(Number.isInteger(body.year_fit)){
		filter.push({ "range": { "rocznik_pas_do": { "gte": body.year_fit }}});
		filter.push({ "range": { "rocznik_pas_od": { "lte": body.year_fit }}});
	}

	//adding filters
	if(Number.isInteger(body.group))
		filter.push({ "term": 
			{'id_grupa': body.group}}); 
	if(Number.isInteger(body.model))
		filter.push({ "term": 
			{'id_model': body.model}});
	if(Number.isInteger(body.make))
		filter.push({ "term": 
			{'id_marka': body.make}}); 
	if(Number.isInteger(body.year))
		filter.push({ "term": 
			{'rocznik': body.year}});


	if(body.price){
		if(Number.isInteger(body.price.eq))
			filter.push({ "term": {'cena_proponowana': body.price.eq}});
		if(Number.isInteger(body.price.from))
			filter.push({ "range": { "cena_proponowana": { "gte": body.price.from }}});
		if(Number.isInteger(body.price.to))
			filter.push({ "range": { "cena_proponowana": { "lte": body.price.to }}});
	}

  let must =  {"must": {
      "multi_match": {
        "query": body.text
				,"fields": [ "komentarz_cz", "nazwy_czesci.nazwa_czesci", "modele_samochodow.name", "marki_samochodow.name"]
      }
    }
  };

  if(body.text)	query_p.body.query.bool = must;
  query_p.body.query.bool.filter = filter;

	//adding offset and limit
  if(query.limit) 	query_p.size = query.limit;
	if(query.offset) 	query_p.from = query.offset;

	query_p.index = 'parts';
  query_p.type =  'part';
	
	/*  
	if(body.order)
    	query_params.order = body.order;
  	*/

	return await client.search(query_p)
	  	.then((body) =>  {return body.hits})//res.json(body)})//.hits.hits);
	  	.catch(e => {throw e});
}



function isEmptyObject(obj) {
  return !Object.keys(obj).length;
}

module.exports = { parts_query };