const Sequelize = require('sequelize');
const database  = require('../../../services/ares_db').database //tmp database!!
const Op        = Sequelize.Op;


//part schema
const Part = database.define('czesci', {

  id_czesci:{
    type: Sequelize.INTEGER
    ,primaryKey: true
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,id_model:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,id_marka:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }
 
  ,data_wpisu:{
    type: Sequelize.DATE

    ,validate: {
      isDate: true
    }
  }  

  ,rocznik_pas_od:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }  

  ,rocznik_pas_do:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }  

  ,rocznik:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }  

  ,waga_czesci:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }    

  ,id_nazwa:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,id_grupa:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,cena_nowej:{
    type: Sequelize.DOUBLE

    ,validate: {
      isNumeric: true
    }
  }

  ,cena_proponowana:{
    type: Sequelize.DOUBLE

    ,validate: {
      isNumeric: true
    }
  }

  ,id_polki:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,komentarz_cz:{
    type: Sequelize.STRING(8104) 
  }
   
  ,przebieg:{
    type: Sequelize.DOUBLE

    ,validate: {
      isNumeric: true
    }
  }

  ,numer_kat:{
    type: Sequelize.STRING(16) 
  }

  ,rezerwacja:{
    type: Sequelize.BOOLEAN
  }

  ,rez_dla:{
    type: Sequelize.STRING(16) 
  } 
 
  ,nr_ewid:{
    type: Sequelize.INTEGER

    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,pochodzenie_zew:{
    type: Sequelize.STRING(20) 
  } 

});

//static class methods

/**
 * Get part by id
 * @property {number} id - Parts id
 * @returns {part}
  */
Part.get = async function(id){  
  var response = await Part.findById(id)
    .then((part) => {
      return part; // eslint-disable-line no-param-reassign
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
Part.list = async function(range){
    range.order = Sequelize.col('id_czesci')//adding order by id to findAll params
    return await this.findAll(range)
      .then(parts => {return parts})
      .catch(err => {throw err})
  }

/**
 * Checks wheret the part is available or booked.
 * @property {partId} partId Parts id.
 * @returns {bool} returns true if available, false if booked  
 */
Part.available = async function(partId){
  return  await Part.findById(partId)
    .then((part) => {
        if(!part) return false;
        else return !part.rezerwacja;
    })
    .catch(err => {throw err})
} 



//instance methods

//create or check if table exiest
//Part.sync();

module.exports = { Part }