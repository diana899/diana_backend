const Payments = require('./model');
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler = require('../../../services/diana_db').errHandler;



function load(req, res, next, id) {
	Payments.get(id)
		.then(payments => {
			req.id = id;
			req.payments = payments; 
			next();
		})
		.catch(e => next(e));
}


function get(req, res) {
	if(req.payments)
		res.status(200).json(req.payments);
	else
		res.status(404).send('No payments with id: ' + req.id + ' in dictionairy');
}


function create(req, res, next) {
	const payments = new Payments(req.body);
	payments.save()
		.then(savedPayment => res.json(savedPayment))
		.catch(err => next(dbErrHandler(err)));
}


function update(req, res, next) {
	let payments = req.payments;
	payments.update(req.body)
		.then(savedPayment => {res.json(savedPayment)})
		.catch(err => next(dbErrHandler(err)));   
}


function list(req, res, next) {
	Payments.list()
		.then(payments => res.json(payments))
		.catch(err => next(dbErrHandler(err)));
}


function remove(req, res, next) {
	const payments = req.payments;
	payments.destroy()
		.then(deletedPayment => res.json(deletedPayment))
		.catch(e => next(e));    
}



module.exports = { load, get, create, update, list, remove };