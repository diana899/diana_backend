const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database

/** @see Complaints */
/**
 * AudComplaints type
 * @typedef {Object} module:Complaints.AudComplaints
 * @property {number} ComplaintsId unique
 * @property {char} DComplaintStatusesId
 * @property {date} Date
 */
const AudComplaints = database.define('AudComplaints', {

  ComplaintsId:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,DComplaintStatusesId:{
    type: Sequelize.STRING(1)
    ,allowNull: false
  }

  ,Date: Sequelize.DATE

});

//static class methods

/**
 * Get part by id
 * @property {number} id - Parts id
 * @returns {part}
  */
AudComplaints.get = async function(id){  
  var response = await AudComplaints.findById(id)
    .then((audComplaint) => {
      return audComplaint; // eslint-disable-line no-param-reassign
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
AudComplaints.list = async function(range){
    range.order = Sequelize.col('Id')//adding order by id to findAll params
    return await AudComplaints.findAll(range)
      .then(audComplaints => {return audComplaints})
      .catch(err => {throw err})
  }


//instance methods

module.exports = AudComplaints