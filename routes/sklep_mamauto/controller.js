const md5 	= require('md5');

const model 	= require('../orders/model');
const aresApi 	= require('../../services/ares_api')

function handleAresRequest(req, res, next) {
	var { auth, token, magazyn, command, start, limit, idorig } = req.query;
	if(auth && token && magazyn === 'mamAUTO'){
		if(aresApi.authAres(auth, token)){
			let commandHandler = decodeAresCommand(command); //get command handler function
			commandHandler(req, res, next, {start, limit, idorig});  //pass controll to command handle function
		}
		else
			res.status(403).send('Forbidden');
	}
	else
		res.status(403).send('Forbidden');
}

function wrongCommand(req, res, next, data){
	res.status(404).send('No such command');
}

function deletepart(req, res, next, data){
	res.status(200).send('OK');
}

async function getDealsCart(req, res, next, data){
	let orderId = data.start;
	if(orderId){
		model.Orders.order(orderId)
			.then(order => aresApi.buildOrderAresResponse(order))
			.then(response => res.status(200).send(response))
			.catch(e => {next(e)})
	}else
		next({message:'no order id'})
}

function getDealsSer(req, res, next, data){
	let orderId = data.start;
	let limit 	= data.limit;
	if(orderId){
		model.ordersFrom(orderId, limit)
			.then(orders => aresApi.formatDealItems(orders))
			.then(response => res.status(200).json(response))
			.catch(e => {next(e)})
	}else
	next({message:'no order id'})
}

function decodeAresCommand(command){	
	switch(command){
		case 'deletepart':
			return deletepart;
		case 'getdealscart':
			return getDealsCart;
		case 'getdealsser':
			return getDealsSer;
		default:
			return wrongCommand;
	}
}



module.exports = { handleAresRequest };