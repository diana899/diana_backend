const Sequelize = require('sequelize');
const database = require('../../../services/ares_db').database //tmp database!!


//part_car_make schema
const Part_car_make = database.define('marki_samochodow', {

  id_marki:{
    type: Sequelize.INTEGER
    ,primaryKey: true
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,nazwa_marki:{
    type: Sequelize.STRING(16) 
    ,allowNull: false
  }

});

//static class methods

/**
 * Get part_car_make by id
 * @property {number} id - Parts id
 * @returns {part_car_make}
  */
Part_car_make.get = async function(id){  
  var response = await Part_car_make.findById(id)
    .then((part_car_make) => {
      return part_car_make;
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part_car_make list within range.
 * @property {number} skip - Number of part_car_makes to be skipped.
 * @property {number} limit - Limit number of part_car_makes to be returned.
 * @returns {part_car_make[]}
  */
Part_car_make.list = async function(range){
    range.order = Sequelize.col('id_marki')//adding order by id to findAll params
    return await this.findAll(range)
      .then(part_car_makes => {return part_car_makes})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//Part_car_make.sync();

module.exports = { Part_car_make }