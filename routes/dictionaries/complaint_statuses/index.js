const express = require('express');
const controller = require('./controller.js');

const router = express.Router();

/** @see Dictionaries */

/** @namespace Dictionaries.api/dictionaries/complaintStatus/
 */
router.route('/')
	/** Get list of complaint status from dictionary
	 * @function
	 * @memberof Dictionaries.api/dictionaries/complaintStatus/
	 * @name GET /api/dictionaries/complaintStatus/
	 * @returns {complaintStatus[]} list of [complaintStatus]{@link module:Dictionaries.complaintStatus}
	 */
	.get(controller.list)
	/** 
	 * Create new status
	 * @function
	 * @memberof Dictionaries.api/dictionaries/complaintStatus/
	 * @name POST /api/dictionaries/complaintStatus/
	 * @param {complaintStatus} [complaintStatus]{@link module:Dictionaries.complaintStatus} BODY
	 * @returns {complaintStatus|Error} [complaintStatus]{@link module:Dictionaries.complaintStatus}
	*/
	.post(controller.create);

router.route('/:statusId')
	/** 
	* Get status by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/complaintStatus/
	* @name GET /api/dictionaries/complaintStatus/
	* @param {number} statusId PARAM
	* @returns {complaintStatus|Error} [complaintStatus]{@link module:Dictionaries.complaintStatus}
	*/
	.get(controller.get)
	/** 
	* Update status by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/complaintStatus/
	* @name PUT /api/dictionaries/complaintStatus/
	* @param {number} statusId PARAM
	* @param {complaintStatus} complaintStatus - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {complaintStatus|Error} [complaintStatus]{@link module:Dictionaries.complaintStatus}
	*/
	.put(controller.update)
	/** 
	* Delete status by symbol
	* @memberof Dictionaries.api/dictionaries/complaintStatus/
	* @name DELETE /api/dictionaries/complaintStatus/
	* @function
	* @param {number} statusId PARAM
	* @returns {complaintStatus|Error} [complaintStatus]{@link module:Dictionaries.complaintStatus}
	*/
	.delete(controller.remove);

///** Load status when API route parameter is hit */
router.param('statusId', controller.load);




module.exports = router;