const express = require('express');
const controller = require('./controller.js');

const router = express.Router();

/**  @see Dictionaries */

/** @namespace Dictionaries.api/dictionaries/payments/ */
router.route('/')
	/** Get list of payments from dictionary
	 * @function
	 * @memberof Dictionaries.api/dictionaries/payments/
	 * @name GET /api/dictionaries/payments/
	 * @returns {Payment[]} list of [Payment]{@link module:Dictionaries.Payment}
	 */
	.get(controller.list)
	/** 
	 * Create new payment
	 * @function
	 * @memberof Dictionaries.api/dictionaries/payments/
	 * @name POST /api/dictionaries/payments/
	 * @param {Payment} [Payment]{@link module:Dictionaries.Payment} BODY
	 * @returns {Payment|Error} [Payment]{@link module:Dictionaries.Payment}
	*/
	.post(controller.create);

router.route('/:paymentId')
	/** 
	* Get payment by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/payments/
	* @name GET /api/dictionaries/payments/
	* @param {number} statusId PARAM
	* @returns {Payment|Error} [Payment]{@link module:Dictionaries.Payment}
	*/
	.get(controller.get)
	/** 
	* Update payment by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/payments/
	* @name PUT /api/dictionaries/payments/
	* @param {number} statusId PARAM
	* @param {Payment} Payment - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {Payment|Error} [Payment]{@link module:Dictionaries.Payment}
	*/
	.put(controller.update)
	/** 
	* Delete payment by symbol
	* @memberof Dictionaries.api/dictionaries/payments/
	* @name DELETE /api/dictionaries/payments/
	* @function
	* @param {number} statusId PARAM
	* @returns {Payment|Error} [Payment]{@link module:Dictionaries.Payment}
	*/
	.delete(controller.remove);

///** Load payment when API route parameter is hit */
router.param('paymentId', controller.load);




module.exports = router;