const { EventEmitter } = require('events')
const app = require('../bin/www')
const database = require('../services/diana_db')

EventEmitter.defaultMaxListeners = Infinity
jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000

global.Array = Array
global.Date = Date
global.Function = Function
global.Math = Math
global.Number = Number
global.Object = Object
global.RegExp = RegExp
global.String = String
global.Uint8Array = Uint8Array
global.WeakMap = WeakMap
global.Set = Set
global.Error = Error
global.TypeError = TypeError
global.parseInt = parseInt
global.parseFloat = parseFloat


//beforeAll(async () => {await mockgoose(mongoose)mongoose.connect(mongo.uri)})
afterAll(() => {
  app.close();
  database.close()
})