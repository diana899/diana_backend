var dotenv 			= require('dotenv').config()
var express 		= require('express');
var path 			= require('path');
var logger 			= require('morgan');
var cookieParser 	= require('cookie-parser');
var bodyParser 		= require('body-parser');
var passport 		= require("passport");
var jwt 			= require('jsonwebtoken');
var helmet 			= require('helmet');

var strategy 	= require('./services/authentication').strategy;
var { limiter, createAccountLimiter, logginLimiter } = require('./services/limiter');

var index 		= require('./routes/index');
var users 		= require('./routes/users/index');
var parts 		= require('./routes/parts/index');
var orders 		= require('./routes/orders/index');
var admins 		= require('./routes/admins/index');
var complaint 	= require('./routes/complaints/index');
var miniapi 	= require('./routes/sklep_mamauto/index');

var statuses 	= require('./routes/dictionaries/statuses');
var addresses 	= require('./routes/dictionaries/addresses');
var payments 	= require('./routes/dictionaries/payments');
var shippings	= require('./routes/dictionaries/shippings');
var emails 		= require('./routes/dictionaries/emails');
var synonymes 	= require('./routes/dictionaries/synonymes');
var complaintStatuses = require('./routes/dictionaries/complaint_statuses');

var app 	= express();

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-real-ip, x-forwarded-for');
	next();
});

app.use(helmet());

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

passport.use(strategy);
app.use(passport.initialize());

//rate limmiters
app.use(limiter); //light limit for all routes
app.use('/api/users/login/',logginLimiter); //hard limit for creating account
app.post('/api/users/', createAccountLimiter); //email limit, on failed loggin

//----    MAIN ROUTES
app.use('/api/', 			index);
app.use('/api/users', 		users);
app.use('/api/parts', 		parts);
app.use('/api/orders', 		orders);
app.use('/api/admins', 		admins);
app.use('/api/complaints', 	complaint);
app.use('/sklep_mamauto', 	miniapi);

//----    DICTIONARY ROUTES
app.use('/api/dictionaries/statuses', 	statuses);
app.use('/api/dictionaries/addresses', 	addresses);
app.use('/api/dictionaries/payments', 	payments);
app.use('/api/dictionaries/shippings', 	shippings);
app.use('/api/dictionaries/emails', 	emails);
app.use('/api/dictionaries/synonymes', 	synonymes);
app.use('/api/dictionaries/complaintStatuses', complaintStatuses);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.send({status: (err.status || 500), message: err.message});
});


module.exports = app;