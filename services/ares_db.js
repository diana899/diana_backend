const Sequelize = require('sequelize');
const AresConfig = require('../configs/ares.json')
//connection to database

AresConfig.host = process.env.ARES_HOST
AresConfig.port = process.env.ARES_PORT


const database = new Sequelize(
  process.env.ARES_SCHEMA   //schema name
  ,process.env.ARES_USER    //database user
  ,process.env.ARES_PASS    //database password

  ,AresConfig               //data from configuration file

);







//database methods ECONNREFUSED - cannot connect
function errHandler(err){
  const httpErr = { //default err status
    status  : 500,
    message : 'Internal database error'
  };  
  //common error decode
  if(err.parent)
    switch(err.parent.code){
      case 'ECONNREFUSED':
        httpErr.message = 'Cannot connect to database';
        break;
      default:
        break;
    }
  if(err.name)
    httpErr.message =  err.errors[0].message

  return httpErr;
};


async function testConnection(){
  let res;
  await database
    .authenticate()
    .then(() => {
      res = res.status(200).send('Connection has been established successfully.');
    })
    .catch(err => {
      res = res.status(500).send('Cannot connect to database.');
    }); 
    return res
}


module.exports = { database, testConnection , errHandler}