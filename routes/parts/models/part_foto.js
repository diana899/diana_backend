const Sequelize = require('sequelize');
const database = require('../../../services/ares_db').database //tmp database!!


//part schema
const Part_foto = database.define('czesci_foto', {

  id_czesci:{
    type: Sequelize.INTEGER
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,foto_link:{
    type: Sequelize.STRING(128) 
  }

});

Part_foto.removeAttribute('id');

//static class methods

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
Part_foto.list = async function(range){  
    range.order = Sequelize.col('id_czesci')//adding order by id to findAll params
    return await this.findAll(range)
      .then(part_foto => {return part_foto})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//Part_name.sync();

module.exports = { Part_foto }