const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database; //tmp database!!


/** @see Users */
/**
 * UserAddress type
 * @typedef {Object} module:Users.UserAddress 
 * @property {string} DAddressesId
 * @property {string=} Email Validated
 * @property {string=} Name
 * @property {string=} LastName
 * @property {string=} CompanyName
 * @property {string=} PostalCode
 * @property {string=} City
 * @property {string=} Address
 * @property {string=} PhoneNumber Validated
 */
const UserAddresses = database.define('UserAddresses', {

	UsersId:{
		type: Sequelize.INTEGER
		,allowNull: false
		,validate: {
			notEmpty: true
			,isNumeric: true
			,isInt: true
		}
	}

	,DAddressesId: {
		type: Sequelize.STRING(1)
		,allowNull: false
		,defaultValue: 'M'
		,validate: {
			notEmpty: true
		}
	}

	,Email: {
		type: Sequelize.STRING(64)
		,validate: {
			isEmail: true
		}
	}

	,Name: {
		type: Sequelize.STRING(32)
	}

	,LastName: {
		type: Sequelize.STRING(32)
	}

	,CompanyName: {
		type: Sequelize.STRING(50)
	}

	,NIP: {
      type: Sequelize.STRING(10)
	}

	,PostalCode: {
		type: Sequelize.STRING(6)
	}

	,City: {
		type: Sequelize.STRING(32)
	}

	,Address: {
		type: Sequelize.STRING(100)
	}

	,PhoneNumber: {
		type: Sequelize.STRING(100)
		,validate: {
			len: [7,12] //minimum lenght of password
			,is: ['^[0-9]*$'] //allow only numbers
		}
	}

	,IsCompany: {
		type: Sequelize.BOOLEAN 
		,defaultValue: false
	}

});

//static class methods

/**
 * Get part_car_model by id
 * @property {number} id - Parts id
 * @returns {part_car_model}
  */
UserAddresses.get = async function(id){  
	var response = await orderAddresses.findById(id)
		.then((orderAddresses) => {
			return part_car_model;
		})
		.catch(err => {throw err;});

	return response;
};

/**
 * Get part_car_model list within range.
 * @property {number} skip - Number of part_car_models to be skipped.
 * @property {number} limit - Limit number of part_car_models to be returned.
 * @returns {part_car_model[]}
  */
UserAddresses.list = async function(range){
	range.order = Sequelize.col('Id');//adding order by id to findAll params
	return await this.findAll(range)
		.then(orderAddresses => {return orderAddresses;})
		.catch(err => {throw err;});
};


//instance methods

//create or check if table exiest
//Part_car_model.sync();

module.exports = UserAddresses;