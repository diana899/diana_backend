const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database

/** @see Dictionaries */

/**
 * Synonyme type
 * @typedef {Object} module:Dictionaries.Synonymes
 * @property {string} Word
 * @property {string} Synonyme
 */
const DSynonymes = database.define('DSynonymes', {

  Word:{
    type: Sequelize.STRING(64) 
    ,allowNull: false
    ,validate: {
    	notEmpty: true
    }
  }

  ,Synonyme:{
    type: Sequelize.STRING(64) 
  }
});

//static class methods
DSynonymes.get = async function(id){  
  var response = await DSynonymes.findById(id)
    .then((synonyme) => {
      return synonyme;
    })
    .catch(err => {throw err})

    return response
  }

DSynonymes.list = async function(){
    
    return await this.findAll({order : Sequelize.col('id')})
      .then(synonymes => {return synonymes})
      .catch(err => {throw err})
  }


//creating dictionaries (if not exiests)
DSynonymes.sync();


module.exports = DSynonymes