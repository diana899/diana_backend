const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
//tables //TODO pobierac modele z innego miejsca

/** @see Dictionaries */

/**
 * Email type
 * @typedef {Object} module:Dictionaries.Email
 * @property {string} Symbol UNIQUE
 * @property {string} Name
 * @property {string=} Comment
 * @property {string} Subject emails subject
 * @property {string=} Text html formated email text
 */
const DEmails = database.define('DEmails', {

  Symbol:{
    type: Sequelize.STRING(26) 
    ,allowNull: false
    ,validate: {
    	notEmpty: true
    }
  }

  ,Name:{
    type: Sequelize.STRING(64) 
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Comment:{
    type: Sequelize.STRING(256) 
  }

  ,Subject:{
    type: Sequelize.STRING(256) 
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Text:{
    type: Sequelize.TEXT 
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

});

//static class methods
DEmails.get = async function(id){  
  return await DEmails.findById(id)
    .then((dEmail) => {
      return dEmail;
    })
    .catch(err => {throw err})
  }

//static class methods
DEmails.getBySymbol = async function(symbol){ 
  let qr = {
    where: {
      Symbol: symbol
    }
  };
  return await DEmails.findOne(qr)
    .then((dEmail) => {
      return dEmail;
    })
    .catch(err => {throw err})
  }

DEmails.list = async function(){
    return await this.findAll({order : Sequelize.col('id')})
      .then(dEmails => {return dEmails})
      .catch(err => {throw err})
  }

//creating dictionaries (if not exiests)
DEmails.sync();


module.exports = DEmails