const Sequelize = require('sequelize');
const database = require('../../../services/ares_db').database //tmp database!!


//part_group schema
const Part_group = database.define('grupy_czesci', {

  id_grupa:{
    type: Sequelize.INTEGER
    ,primaryKey: true
    ,allowNull: false

    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,nazwa_grupy:{
    type: Sequelize.STRING(64) 
  }

});

//static class methods

/**
 * Get part_group by id
 * @property {number} id - Parts id
 * @returns {part_group}
  */
Part_group.get = async function(id){  
  var response = await Part_group.findById(id)
    .then((part_group) => {
      return part_group;
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part_group list within range.
 * @property {number} skip - Number of part_groups to be skipped.
 * @property {number} limit - Limit number of part_groups to be returned.
 * @returns {part_group[]}
  */
Part_group.list = async function(range){
    range.order = Sequelize.col('id_grupa')//adding order by id to findAll params
    return await this.findAll(range)
      .then(part_groups => {return part_groups})
      .catch(err => {throw err})
  }


//instance methods

//create or check if table exiest
//Part_group.sync();

module.exports = { Part_group }