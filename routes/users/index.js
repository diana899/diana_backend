const express = require('express');
const controller = require('./controller.js');


var requireAuthACL 		 = require('./../../services/authentication').requireAuthACL;
var createAccountLimiter = require('./../../services/limiter').createAccountLimiter;

const router = express.Router();


/** Express router providing user related routes
 * @module Users
 */

/** @namespace Users.api/users/ */
router.route('/')
	/** Get list of users
	 * @function
	 * @memberof Users.api/users/
	 * @name GET /api/users/
	 * @returns {User[]} list of [User]{@link module:Users.User}
	 */
	.get(requireAuthACL(canUser = false, 'user_list'), controller.list)
	/** 
	 * Create new user
	 * @function
	 * @memberof Users.api/users/
	 * @name POST /api/users/
	 * @param {User} User BODY
	 * @returns {User|Error} [User]{@link module:Users.User}
	*/
	.post(requireAuthACL(canUser = true, 'create_user'), controller.create);

router.route('/:userId')
	/** 
	* Get user by id
	* @function
	* @memberof Users.api/users/
	* @name GET /api/users/
	* @param {number} userId PARAM
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.get(requireAuthACL(canUser = true, 'get_user'), controller.get)
	/** 
	* Update users data by id
	* @function
	* @memberof Users.api/users/
	* @name PUT /api/users/
	* @param {number} userId PARAM
	* @param {User} User - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.put(requireAuthACL(canUser = true, 'update_user'), controller.update)
	/** 
	* Delete user by id
	* @memberof Users.api/users/
	* @name DELETE /api/users/
	* @function
	* @param {number} userId PARAM
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.delete(requireAuthACL(canUser = true, 'delete_user'), controller.remove);


/** @namespace Users.api/users/activate/ */
router.route('/activate/key=:key')
	/** Activate user by activation key
	* @function
	* @memberof Users.api/users/activate/
	* @name GET /api/activate/key=
	* @param {string} activationKey PARAM
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.get(requireAuthACL(canUser = true), controller.activate);

///OTHER ROUTS
/** @namespace Users.api/users/login/ */
router.route('/login/')
	/** 
	* Login user
	* @function
	* @memberof Users.api/users/login/
	* @name POST /api/users/login/
	* @param {Object} Body BODY
	* @param {string} Body.Email
	* @param {string} Body.Password
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.post(requireAuthACL(canUser = true), controller.login);

/** @namespace Users.api/users/address/ */
router.route('/address/:userId')
	/** 
	* Delete users address by id and type of address
	* @function
	* @memberof Users.api/users/address/
	* @name DELETE /api/users/address/
	* @param {number} userId PARAM
	* @param {Object} Body QUERY
	* @param {char} Body.DAddressesId type of address (fk to DAdresses)
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.delete(requireAuthACL(canUser = true, 'update_user'), controller.delete_addr);

/** @namespace Users.api/users/password/ */
router.route('/password/:userId')
	/** 
	* Change users password
	* @function
	* @memberof Users.api/users/password/
	* @name PUT /api/users/password/
	* @param {number} userId PARAM
	* @param {Object} Body BODY
	* @param {char} Body.PasswordOld 
	* @param {char} Body.PasswordNew
	* @returns {User|Error} [User]{@link module:Users.User}
	*/
	.put(requireAuthACL(), controller.change_pass);

/** @namespace Users.api/users/anal/ */
router.route('/list/anal/')
	/** Get list of users
	 * @function
	 * @memberof Users.api/users/anal/
	 * @name POST /api/users/anal/
	 * @param {string[][]} [Order] QUERY
	 * @returns {UserAddress[]} list of [UserAddress]{@link module:Users.UserAddress}
	 */
	.post(requireAuthACL(canUser = false, 'user_list_anal'), controller.listAnal)


///PARAMETERS
///** Load user when API with userId route parameter is hit */
router.param('key', controller.loadByKey);


///** Load user when API with activationKey route parameter is hit */
router.param('userId', controller.load);




module.exports = router;