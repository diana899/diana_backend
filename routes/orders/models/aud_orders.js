const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
const dateFormat = require('dateformat');

/** @see AudOrders */
/**
 * AudOrders type
 * @typedef {Object} module:Orders.AudOrders
 * @property {string} Symbol unique AUTO
 * @property {number} NumberInMonth AUTO
 * @property {string} AdminsId ID of admin who did last modification
 * @property {char} DStatusesId FK to DStatuses
 * @property {date} Date cration date
 * @property {date} ModDate modification date
 * @property {char!} DShippingsId FK to DShippings
 * @property {string} ShippingMethod name of shipping method
 * @property {number} ShippingPrice price of shipping method
 * @property {char!} DPaymentsId FK to DPayments
 * @property {string=} UsersId FK to DShippings
 * @property {OrderElements[]} OrderElements
 * @property {OrderAddresses[]} OrderAddresses
 */
const AudOrders = database.define('AudOrders', {

  OrdersId:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      notEmpty: true
      ,isNumeric: true
      ,isInt: true
    }
  }

  ,DStatusesId:{
    type: Sequelize.STRING(1)
    ,allowNull: false
    ,defaultValue: 'N'
  }

  ,Date: Sequelize.DATE

});

//static class methods

/**
 * Get part by id
 * @property {number} id - Parts id
 * @returns {part}
  */
AudOrders.get = async function(id){  
  var response = await Orders.findById(id)
    .then((orders) => {
      return orders; // eslint-disable-line no-param-reassign
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
AudOrders.list = async function(range){
    range.order = Sequelize.col('Id')//adding order by id to findAll params
    return await this.findAll(range)
      .then(orders => {return orders})
      .catch(err => {throw err})
  }





//instance methods

//create or check if table exiest
//Part.sync();

module.exports = AudOrders