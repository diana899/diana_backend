const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database;
const dbErrHandler = database.errHandler;


/** @see Users */
/**
 * User type
 * @typedef {Object} module:Users.User
 * @property {string} Email Validated
 * @property {string} Password Encrypted
 * @property {UserAddress[]} UserAdresses
 */
const Users = database.define('Users', {

	Email: {
		type: Sequelize.STRING(64)
		,unique: true 
		,allowNull: false
		,validate: {
			isEmail: true,
			notEmpty: true
		}
	}

	,Password: {
		type: Sequelize.STRING(60)
		,allowNull: false
		,validate: {
			notEmpty: true
		}
	},

	IsActive: {
		type: Sequelize.CHAR
	}

	,ActivationKey: {
		type: Sequelize.STRING(50)
		,unique: true 
		,allowNull: false
		,validate: {
			notEmpty: true
			,len: [50,50]
		}
	}

});


//static class methods

/**
 * Get user by id
 * @property {number} id - Users id
 * @returns {user}
  */
Users.get = async function(id){  
	var response = await Users.findById(id)
		.then((user) => {
			return user;
		})
		.catch(err => {throw dbErrHandler(err);});
	return response;
};

/**
 * Get user by activation key
 * @property {string} ActivationKey - Users ActivationKey
 * @returns {user}
  */
Users.getByKey = async function(key){  
	return await Users.findOne({attributes: {
		exclude: ['Password']
	}
		,where: { ActivationKey: key}})
		.then((user) => {
			return user;
		})
		.catch(err => {throw dbErrHandler(err);});
};

/**
 * Login
 * @property {string} Email - Users Email
 * @property {string} Password - Users encrypted Password
 * @returns {user}
  */
Users.login = async function(email){
	return await Users.findOne(
		{where: { 
			Email: email
		}})
		.then((user) => {
			return user;
		})
		.catch(err => {throw dbErrHandler(err);});
};

/**
 * Get user list within range.
 * @property {number} skip - Number of users to be skipped.
 * @property {number} limit - Limit number of users to be returned.
 * @returns {user[]}
  */
Users.list = async function(range){
	let order = Sequelize.col('id');//adding order by id to findAll params

	let params = {
		order : Sequelize.col('id')
		,attributes: {
			exclude: ['Password', 'ActivationKey']
		}
	};

	return await this.findAll(params)
		.then(users => {return users;})
		.catch(err => {throw dbErrHandler(err);});
};


//instance methods

/**
 * Activate user
 * @returns {user}
  */
Users.prototype.activate = async function(){
	//if user exists, proceed
	if(this.IsActive === 'F'){ //if its not activated
		this.IsActive = 'T'; //set to active
		return await this.save() //save changes to database
			.then(savedUser => {return savedUser;}) //return activated user
			.catch(err => {throw dbErrHandler(err);});
	}
	else
		throw {status: 200, message: 'User already activated'}; 
};


module.exports = Users;