const randomstring 	= require('randomstring');
const bcrypt		= require('bcrypt');
//TODO zastanowic sie, czy przeniesc obsluge bledow bazo danowych w calosci do modelu
const dbErrHandler 	= require('../../services/diana_db').errHandler;
const emailer 		= require('../../services/email');
const Passwords 	= require('../../services/passwords');

const Users = require('./model').Users;
const UserAddresses = require('./model').UserAddresses;

var getToken = require('./../../services/authentication').getToken;

const saltRounds = 10;

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
	Users.user(id)
		.then(user => {
			req.id = id;
			req.user = user; 
			next();
		})
		.catch(e => next(e));
}

/**
 * Load user by activation key and append to req.
 */
function loadByKey(req, res, next, key) {
	Users.getByKey(key)
		.then(user => {
			req.key = key;
			req.user = user;
			next();
		})
		.catch(e => next(e));
}


/**
 * Get user
 * @returns {Users}
 */
function get(req, res) {
	if(req.user)
		res.status(200).json( userSafeView(req.user));
	else
		res.status(404).send('No such user with id: ' + req.id);
}

/**
 * Create new user
 * @property {string} req.body - The user.
 * @returns {Users} 
  */
async function create(req, res, next) {
	const user = new Users({
		Email:          req.body.Email,
		Password:       req.body.Password,
		IsActive:       'F',
		ActivationKey:  randomstring.generate(50), //generate random string
		createdAt: 		new Date(),
		UserAddresses: 	req.body.UserAddresses
	});
	try{
		user.Password 	= await Passwords.encrypt(user.Password);
		req.savedUsers 	= await user.save();
		if(req.body.UserAddresses){
			let val = await req.savedUsers.updateAdresses(req.body.UserAddresses)
			req.savedUsers.dataValues.UserAddresses = val; //append user adresses into user object
		}
		console.log(req.savedUsers.UserAddresses);
		sendConfirm(req, res, next);
	}catch(err){
		next(err);
	}	
}

async function sendConfirm(req, res, next){
	let link = 'http://sklep.mamauto.pl/aktywacja/' + req.savedUsers.ActivationKey;
	await emailer.EmailTemplates.confirmEmial(req.savedUsers.Email, link)
		.then(email => {
			console.log(email);
			emailer.sendEmail(email); //send activation emial
			res.json(userNoPass(req.savedUsers));
		})
		.catch(e => next(e));
}

/**
 * Update existing user
 * @property {string} req.body.Nickname - The Nickname of user.
 * @property {string} req.body.Email    - The Email of user.
 * @property {string} req.body.Name     - The Name of user.
 * @property {string} req.body.LastName - The LastName of user.
 * @returns {user}
 */
function update(req, res, next) {
	let user = req.user;    //get user from load method
	//premove uneditable data
	if(req.body.IsActive)       delete(req.body.IsActive);
	if(req.body.ActivationKey)  delete(req.body.ActivationKey);
	if(req.body.Password)       delete(req.body.Password);

	user.update(req.body)
		.then(savedUsers => {
			Users.update_adr(savedUsers)
				.catch(err => next(err));
			res.status(200).json(userSafeView(savedUsers));
		})
		.catch(err => next(err));   
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {user[]}
 */
function list(req, res, next) {
  let qr = {};
  if(Number.isInteger(parseInt(req.query.limit)))
    qr.limit = parseInt(req.query.limit);
  if(Number.isInteger(parseInt(req.query.offset))) //checking if limit exiests and is number
    qr.offset = parseInt(req.query.offset);
  if(req.query.order)
    qr.order = JSON.parse(req.query.order);
	Users.users(qr) //ORM didnt want to parse int by itself
		.then(users => res.json(users))
		.catch(err => next(err));
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {user[]}
 */
function listAnal(req, res, next) {
  let query = {};
  if(Number.isInteger(parseInt(req.query.limit)))
  	query.limit = parseInt(req.query.limit);
  if(Number.isInteger(parseInt(req.query.offset)))
    query.offset = parseInt(req.query.offset);

  if(req.body.order){	
    query.order = req.body.order;
  }	

	query.where = req.body.where;

	UserAddresses.fullList(query) 
		.then(userAddresses => res.json(userAddresses))
		.catch(err => next(err));
}

/**
 * Delete user.
 * @returns {user}
 */
function remove(req, res, next) {
	const user = req.user;
	user.destroy()
		.then(deletedUsers => res.json(deletedUsers))
		.catch(e => next(e));    
}

/**
 * Activate user with link
 * @returns {user}
 */
function activate(req, res, next) { //  TODO: przeniesc do modelu: instance method
	let user = req.user;
	//if user exists, proceed
	if(user){
		if(user.IsActive === 'F'){ //if its not activated
			user.IsActive = 'T'; //set to activen
			user.save() //save changes to database
				.then(savedUsers => res.redirect('http://sklep.mamauto.pl')
						//{message: ('Użytkownik ' + savedUsers.Email) + ' pomyślnie aktywowany'})
						)
				.catch(err => next(err));
		}
		else
			res.status(500).send('Użytkownik już został aktywowany.');
	}
	else
		res.status(500).send('Zły klucz aktywacyjny');
}


async function login(req, res, next) {
	let err = {status : 404
		,message : 'Zły login lub hasło.'};
	let errActiv = {status : 500
		,message : 'Użytkownik nieaktywny, prosimy potwierdzić email przed logowaniem.'};

	Users.login(req.body.Email)
		.then(user => {
			if(user){
				if(user.IsActive !== 'T')
					throw errActiv;

				Passwords.decrypt(req.body.Password, user.Password)
					.then(resp => {
						if(resp===true){
							user.dataValues.token = getToken({User: user});
							res.status(200).json(userSafeView(user));
						}
						else  
							next(err);
					})
					.catch(e => next(e)); 
			}
			else  
				next(err);    
		})
		.catch(err => next(err));
}

async function change_pass(req, res, next) {
	let err = {status : 404
		,message : 'Wrong password'};
	if(req.user){



		bcrypt.compare(req.body.PasswordOld, req.user.Password).then(resp => {
			if(resp===true){
				req.user.Password = req.body.PasswordNew;
				tmp_change_pass(req, res, next);
			}
			else  
				next(err);
		})
			.catch(err => next(err)); 




	}
	else  
		next(err);
}


function tmp_change_pass(req, res, next){
	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(req.user.Password, salt, function(err, hash) {
			if(!err){
				req.user.Password = hash;
				req.user.save()
					.then(savedUsers => res.json(savedUsers))
					.catch(err => next(dbErrHandler(err))); //    TODO: przenisesc obsluge bledow validacji do innej funkcji
			}
		});
	});
}


////
//personal data and adresses
////
function delete_addr(req, res, next) {
	if(!req.query.DAddressesId)
		next({status:500, message: 'DAddressesId property can not be null'});
	if(req.user)
		req.user.delete_adr(req.query.DAddressesId)
			.then(deletedUserAddress => res.status(200).json(deletedUserAddress))
			.catch(err => next(err));
}


function userNoPass(user){
	if(user.dataValues.Password) delete(user.dataValues.Password);
	if(user.Password) delete(user.Password);
	return user
}

function userSafeView(user){
	user = userNoPass(user);
	if(user.dataValues.ActivationKey) delete(user.dataValues.ActivationKey);
	if(user.ActivationKey) delete(user.ActivationKey);

	return user
}

function userOutView(user){
	user = userSafeView(user);
	if(user.dataValues.id) delete(user.dataValues.id);
	if(user.id) delete(user.id);
	return user
}





module.exports = { load, loadByKey, get, create, update, list, listAnal, remove, activate, login, delete_addr, change_pass};