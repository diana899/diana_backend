const Sequelize   = require('sequelize');
const DianaConfig = require('../configs/diana.json')

DianaConfig.host = process.env.DIANA_HOST
DianaConfig.port = process.env.DIANA_PORT

//connection to database
const database = new Sequelize(
  process.env.DIANA_SCHEMA
  ,process.env.DIANA_USER
  ,process.env.DIANA_PASS

  ,DianaConfig

);






//database methods ECONNREFUSED - cannot connect
function errHandler(err){
  const httpErr = { //default err status
    status  : 500,
    message : 'Internal database error'
  };  
  //common error decode
  if(err.parent)
    switch(err.parent.code){
      case 'ECONNREFUSED':
        httpErr.message = 'Cannot connect to database';
        break;
      default:
        break;
    }

  if(err.name)
    httpErr.message =  err.errors[0].message

  return httpErr;
};


async function testConnection(){
  let res;
  await database
    .authenticate()
    .then(() => {
      res = res.status(200).send('Connection has been established successfully.');
    })
    .catch(err => {
      res = res.status(500).send('Cannot connect to database.');
    }); 

    return res
}


module.exports = { database, testConnection , errHandler}