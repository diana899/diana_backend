//const bcrypt = require('bcrypt');
const passwords = require('../../services/passwords');

describe('decryption tests', () => {

	it('decryption success', () => {
	  expect.assertions(1);
	  return passwords.decrypt('janek899','$2a$10$yfSQKy8yc41H2Rue6MslduFS4Dz4Q3ehSEbs8qNL.x5yojrfRKqcC')
	  	.then(res => expect(res).toEqual(true));
	});

	it('decryption fail', () => {
	  expect.assertions(1);
	  return passwords.decrypt('janek8991','$2a$10$yfSQKy8yc41H2Rue6MslduFS4Dz4Q3ehSEbs8qNL.x5yojrfRKqcC')
	  	.then(res => expect(res).toEqual(false));
	});

	describe('decryption exceptions', () => {

		it('decryption exception, two incorrect parameters', () => {
		  expect.assertions(1);
		  return passwords.decrypt('abc',null)
		  	.catch(e => expect(e.message).toMatch('Hasło nie może być puste'));
		});

		it('decryption exception, first parameter incorrect', () => {
		  expect.assertions(1);
		  return passwords.decrypt('abc',null)
		  	.catch(e => expect(e.message).toMatch('Hasło nie może być puste'));
		});

		it('decryption exception, second parameter incorrect', () => {
		  expect.assertions(1);
		  return passwords.decrypt(null,'bca')
		  	.catch(e => expect(e.message).toMatch('Hasło nie może być puste'));
		});

	});

});

//describe('encryption tests', () => {

	it('encryption success', () => {
	  expect.assertions(1);
	  return passwords.encrypt('janek899')
	  	.then(res => expect(true).toEqual(true))
	});

	it('encryption exception, null password', () => {
	  expect.assertions(1);
	  return passwords.encrypt(null)
	  	.catch(err => expect(true).toEqual(true))
	});

	it('encryption exception, less then 5 character password', () => {
	  expect.assertions(1);
	  return passwords.encrypt('1234')
	  	.catch(err => expect(true).toEqual(true))
	});

//});