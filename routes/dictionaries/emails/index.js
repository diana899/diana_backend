const express = require('express');
const controller = require('./controller.js');

const router = express.Router();

/** @see Dictionaries */

/** @namespace Dictionaries.api/dictionaries/emails/
 */
router.route('/')
	/** Get list of emails from dictionary
	 * @function
	 * @memberof Dictionaries.api/dictionaries/emails/
	 * @name GET /api/dictionaries/emails/
	 * @returns {Email[]} list of [Email]{@link module:Dictionaries.Email}
	 */
	.get(controller.list)
	/** 
	 * Create new email
	 * @function
	 * @memberof Dictionaries.api/dictionaries/emails/
	 * @name POST /api/dictionaries/emails/
	 * @param {Email} [Email]{@link module:Dictionaries.Email} BODY
	 * @returns {Email|Error} [Email]{@link module:Dictionaries.Email}
	*/
	.post(controller.create);

router.route('/:emailId')
	/** 
	* Get email by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/emails/
	* @name GET /api/dictionaries/emails/
	* @param {number} emailId PARAM
	* @returns {Email|Error} [Email]{@link module:Dictionaries.Email}
	*/
	.get(controller.get)
	/** 
	* Update email by symbol
	* @function
	* @memberof Dictionaries.api/dictionaries/emails/
	* @name PUT /api/dictionaries/emails/
	* @param {number} emailId PARAM
	* @param {Email} Email - BODY, undefined fields won't be modified, fields with null will be set to null
	* @returns {Email|Error} [Email]{@link module:Dictionaries.Email}
	*/
	.put(controller.update)
	/** 
	* Delete email by symbol
	* @memberof Dictionaries.api/dictionaries/emails/
	* @name DELETE /api/dictionaries/emails/
	* @function
	* @param {number} emailId PARAM
	* @returns {Email|Error} [Email]{@link module:Dictionaries.Email}
	*/
	.delete(controller.remove);

///** Load email when API route parameter is hit */
router.param('emailId', controller.load);




module.exports = router;