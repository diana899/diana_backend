const express = require('express');
const controller = require('./controller.js')
var requireAuthACL = require('./../../services/authentication').requireAuthACL;


const router = express.Router();

/** Express router providing complaints related routes
 * @module Complaints
 */

///  COMMON actions
/** @namespace Complaints.api/complaints/ */
router.route('/')
	/** Get list of complaints
	 * @function
	 * @memberof Complaints.api/complaints/
	 * @name GET /api/complaints/
	 * @returns {Complaint[]} Complaint list of [Complaint]{@link module:Complaints.Complaint}
	 */
	.get(requireAuthACL(canUser = false, 'complaints_list')
		,controller.list)

router.route('/:complaintId')
	/** 
	* Get complaint by id
	* @function
	* @memberof Complaints.api/complaints/
	* @name GET /api/complaints/
	* @param {number} complaintId PARAM
	* @returns {Complaint|Error} [Complaint]{@link module:Complaints.Complaint}
	*/
	.get(requireAuthACL(canUser = false, 'complaints_get')
		,controller.get)


///  USERs actions
/** @namespace Complaints.api/complaints/user/ */
router.route('/user/:usersId')
	/** 
	* Get order by users id
	* @function
	* @memberof Complaints.api/complaints/user/
	* @name GET /api/complaints/user/
	* @param {number} userId PARAM
	* @returns {Complaint[]|Error} [Complaint]{@link module:Complaints.Complaint}
	*/
	.get(requireAuthACL(canUser = true, 'complaints_user_list')
		,controller.list)
		/**
	 * Create new complaint
	 * @function
	 * @memberof Complaints.api/complaints/
	 * @name POST /api/complaints/
	 * @param {Complaint} [Complaint]{@link module:Complaints.Complaint} BODY
	 * @returns {Complaint|Error} Complaint [Complaint]{@link module:Complaints.Complaint}
	*/
	.post(requireAuthACL(canUser = true, 'complaints_create')
		,controller.create);

///  ADMINs actions
/** @namespace Complaints.api/complaints/status/ */
router.route('/status/:complaintId')
	/**
	* Change complaints status
	* @function
	* @memberof Complaints.api/complaints/status/
	* @name PUT /api/complaints/status/
	* @param {number} complaintId PARAM
	* @param {number[]} status QUERY, pk from DStatusesId
	* @returns {Complaint|Error} [Complaint]{@link module:Complaints.Complaint}
	*/
	.put(requireAuthACL(canUser = false, 'complaints_status')
		,controller.status)

/** @namespace Complaints.api/complaints/message/ */
router.route('/message/:complaintId')
	/**
	* Add message to complaint
	* leave AdminsLogin null if users response
	* @function
	* @memberof Complaints.api/complaints/message/
	* @name POST /api/complaints/message/
	* @param {number} complaintId PARAM
	* @param {ComplaintMessages} message BODY [ComplaintMessages]{@link module:Complaints.ComplaintMessages}
	* @returns {ComplaintMessages|Error} [ComplaintMessages]{@link module:Complaints.ComplaintMessages}
	*/
	.post(requireAuthACL(canUser = true, 'complaints_message')
		,controller.createMessage)

///  COMMON actions
/** @namespace Complaints.api/complaints/list/anal/ */
router.route('/list/anal/')
	/** Get list of complaints
	 * @function
	 * @memberof  Orders.api/complaints/list/anal/
	 * @name GET /api/complaints/list/anal/
	 * @param {Object} where QUERY, complaint status filter
	 * @param {string[][]} order BODY, ordering rules
	 * @returns {Complaint[]} list of [Complaint]{@link module:Complaints.Complaint}
	 */
	.post(requireAuthACL(canUser = true, 'complaints_list_anal')
		,controller.list)


/// PARAMETERS
///** Load complaint when API route parameter is hit */
router.param('complaintId', controller.load_full);

///** Load complaints by users id when API route parameter is hit */
router.param('usersId', controller.load_by_user);


module.exports = router;