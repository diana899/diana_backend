const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
//tables //TODO pobierac modele z innego miejsca
const Orders = require('./../../orders/models/orders');

/** @see Dictionaries*/
/**
 * Payment type
 * @typedef {Object} module:Dictionaries.Payment
 * @property {char} id Symbol of payment (pk)
 * @property {string} Name Name of payment
 * @property {string=} Comment Description of payment
 */
const DPayments = database.define('DPayments', {

  id:{
    type: Sequelize.STRING(1)
    ,primaryKey: true
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Name:{
    type: Sequelize.STRING(32)
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Comment:{
    type: Sequelize.STRING(256) 
  }

  ,Price:{
    type: Sequelize.DOUBLE
    ,validate: {
      isNumeric: true
    }
  }

  ,Icon:{
    type: Sequelize.STRING(256)
  }

});

//static class methods
DPayments.get = async function(id){  
  var response = await DPayments.findById(id)
    .then((dPayment) => {
      return dPayment;
    })
    .catch(err => {throw err})

    return response
  }

DPayments.list = async function(){
    return await this.findAll({order: Sequelize.col('Id')})
      .then(dPayments => {return dPayments})
      .catch(err => {throw err})
  }

//constraints
DPayments.hasMany(Orders, {foreignKey: 'DPaymentsId', targetKey: 'id'})

//creating dictionaries (if not exiests)
DPayments.sync();



module.exports = DPayments