var paypal = require('paypal-rest-sdk');

const PROD            = (process.env.PROD == 'true');
const client_id       = process.env.PAYPAL_ID;
const client_secret   = process.env.PAYPAL_SECRET;

paypal.configure({
    'mode'          :  (PROD?'live':'sandbox')
    ,'client_id'     : client_id
    ,'client_secret' : client_secret
});



async function createPay(create_payment_json){
    return new Promise(function (resolve, reject){
        paypal.payment.create(create_payment_json, function (error, payment) {
            if (error) {
                reject(error);
            } else {
                for (var index = 0; index < payment.links.length; index++) {
                //Redirect user to this endpoint for redirect url
                    if (payment.links[index].rel === 'approval_url') {
                        resolve(payment.links[index].href);
                    }
                }
            }
        }); 
    });
}


async function execPay(paymentId, payerId){
    return new Promise(function (resolve, reject){
        var execute_payment_json = {"payer_id": payerId};
        paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
            if (error) {
                reject(error);
            } else {
                resolve(payment);
            }
        })
    })
}


function createPaymentJson(order){
  if(!order)
    throw {status:500, messageL:'Brak wszystkich elementów zamówienia'}

  var items = order.products();
  let price = order.price()//.fullPrice.toFixed(2);
  let subtotalPrice = price.fullPrice - price.shippingPrice;
  let shippingPrice = price.shippingPrice;
  price = price.fullPrice;
  let link = 'http://sklep.mamauto.pl:3000/api/orders/pay/pal/exec/' + order.id;
  let description = 'Opłata za zamówienia ' + order.Symbol;

  try{
    items = checkItems(items);
  }
  catch(e){
    throw e;
  }

  let create_payment_json = {
    intent: "authorize",
    payer: {
        payment_method: "paypal"
    },
    redirect_urls: {
        return_url: link,
        cancel_url: 'http://cancel.url'
    },
    transactions: [{
        item_list: {
            items: items
        },
        amount: {
            currency: "PLN",
            total: price.toFixed(2),
            details:
              {
                subtotal: subtotalPrice.toFixed(2),
                shipping: shippingPrice.toFixed(2)
              }
        },
        description: description
    }]
  };
  return create_payment_json;
}


function checkItems(items){
  let itemsChecked = [];
  if(!items)
    throw {status:500, messageL:'Brak wszystkich elementów zamówienia'}
  if(items.length==0)
    throw {status:500, messageL:'Brak wszystkich elementów zamówienia'}
  for(let i=0;i<items.length;i++){
    let item = {
      name      : items[i].name
      ,description: items[i].description
      ,price    : ((items[i].price)/100).toFixed(2)
      ,currency : "PLN"     
      ,quantity : items[i].quantity
    }
    itemsChecked.push(item);
  }

  return itemsChecked;
}


module.exports = {execPay, createPay, createPaymentJson }