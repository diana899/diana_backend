const Sequelize = require('sequelize');

const Model = require('./model.js');
const Users = require('./../users/model.js').Users;

const emailer = require('../../services/email');



/// Loading functions
async function load(req, res, next, id) {
  req.id = id
  req.complaint = await Model.Complaints.get(id)
  next();
}

async function load_full(req, res, next, id) {
  req.id = id
  req.complaint = await Model.Complaints.fullGet(id)
  next();
}

async function load_by_user(req, res, next, id) {
    req.body.where = {UsersId: id};
    next();
}

/// Main operations functions
function get(req, res) {
  if(req.complaint)
    res.status(200).send(req.complaint);
  else
    res.status(404).send('No such complaint with id: ' + req.id)
}

function list(req, res, next) {
  let query = {where: {}};
  if(Number.isInteger(parseInt(req.query.limit)))
    query.limit = parseInt(req.query.limit);
  if(Number.isInteger(parseInt(req.query.offset))) //checking if limit exiests and is number
    query.offset = parseInt(req.query.offset);

  if(req.body.order)
    query.order = req.body.order;

  if(req.body.where)
    query.where =req.body.where;

  Model.Complaints.fullList(query)
    .then(complaints => res.json(complaints))
    .catch(err => next(err));
}

function status(req, res, next){
  if(req.complaint){
    req.complaint.setStatus(req.query.status)
      .then(complaint => res.json(complaint))
      .catch(err => next(err));
  }
  else
    res.status(404).send('No such complaint with id: ' + req.id)
}

function create(req, res, next) {
  let complaintRequest = complaintFilter(req.body);
  complaintRequest.UsersId = req.usersId;
  const complaint = new Model.Complaints(complaintRequest);
  complaint.save()
    .then(savedComplaint => {
      complaintMail(savedComplaint)
        .then(res.json(savedComplaint))
        .catch(err => next(err));
    })
    .catch(err => next(err));
}

function createMessage(req, res, next){
  if(req.complaint){
    let cmBody = complaintMessageFilter(req.body);
    cmBody.ComplaintsId = req.complaint.id;
    let complaintMessage = new Model.ComplaintMessages(cmBody)
    complaintMessage.save()
      .then(savedComplaintMessage => res.json(savedComplaintMessage))
      .catch(e => next(e));
  }
  else
    res.status(404).send('No such complaint with id: ' + req.id)
}

/// Email construction and delivery functions
async function complaintMail(complaint){
  let user = await Users.user(complaint.UsersId);
  let emailAddress = await user.getEmail();
  return await emailer.EmailTemplates.newComplaintMail(
      emailAddress
      ,complaint.id
      ,complaint.Date
      ,complaint.id_czesci)
    .then(mail => {return mail})//{emailer.sendEmail(mail)})
    .catch(err => {throw err});
}

// async function complaintStatusChangedMail(order){
//   let emailAddress = await order.getEmail();
//   let price = await order.price().fullPrice.toFixed(2) + 'zł';
//   emailer.EmailTemplates.newOrderEmail(emailAddress,order.Symbol, price, order.Date)
//     .then(mail => emailer.sendEmail(mail))
//     .catch(err => {throw err});
// }


/// MISC
function complaintFilter(complaint){
  if(complaint.DComplaintStatusesId)  delete(DComplaintStatusesId)
  if(complaint.Date)                  delete(Date)
  if(complaint.ModDate)               delete(ModDate)
  if(complaint.ModAdminsLogin)        delete(ModAdminsLogin)
  complaint.Date = new Date();
  return complaint;
}

function complaintMessageFilter(complaintMessage){
  if(complaintMessage.ComplaintsId)  delete(ComplaintsId)
  if(complaintMessage.Date)          delete(Date)
  complaintMessage.Date = new Date();
  return complaintMessage;
}



module.exports = { load, load_full, load_by_user, get, list, status, create, createMessage }