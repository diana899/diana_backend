const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database; //tmp database!!


/** @see Admins */
/**
 * AdminRoles type
 * @typedef {Object} module:Admins.AdminRoles 
 * @property {string} Symbol UNIQUE
 * @property {string} Name
 * @property {string=} Comment
 */
const AdminRoles = database.define('AdminRoles', {

	Symbol: {
		type: Sequelize.STRING(26)
		,unique: true 
		,allowNull: false
		,validate: {
			notEmpty: true
		}
	}

	,Name: {
		type: Sequelize.STRING(64)
		,unique: true 
		,allowNull: false
		,validate: {
			notEmpty: true
		}
	}

	,Comment: {
		type: Sequelize.STRING(256)
	}

});

//static class methods

/**
 * @ignore
 * Get part_car_model by id
 * @property {number} id - Parts id
 * @returns {part_car_model}
  */
AdminRoles.get = async function(id){  
	return await AdminRoles.findById(id)
		.then((adminRole) => {
			return adminRole;
		})
		.catch(err => {throw err;});
};

/**
 * @ignore
 * Get part_car_model list within range.
 * @property {number} skip - Number of part_car_models to be skipped.
 * @property {number} limit - Limit number of part_car_models to be returned.
 * @returns {part_car_model[]}
  */
AdminRoles.list = async function(range){
	range.order = Sequelize.col('id');//adding order by id to findAll params
	return await this.findAll(range)
		.then(adminRoles => {return adminRoles;})
		.catch(err => {throw err;});
};

//instance methods


module.exports = AdminRoles;