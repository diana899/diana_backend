const express = require('express');
const controller = require('./controller.js')

var requireAuthACL = require('./../../services/authentication').requireAuthACL;

const router = express.Router();

/** Express router providing parts routs
 * @module Parts
 */

 /** @see QueryBody */
/**
 * QueryBody type
 * @typedef {Object} module:Parts.QueryBody
 * @property {number=} group Parts group id
 * @property {number=} model Parts model id
 * @property {number=} make Parts make id
 * @property {number=} year Parts year of manufacture
 *
 * @property {Object=} price Parts year of manufacture
 * @property {number=} price.eq Price equals...
 * @property {number=} price.from Price greater than
 * @property {number=} price.to Price Lower than
 *
 * @property {string=} name Search by parts name
 * @property {number=} year_fit Year, part needs to fit
 * @property {string=} text Search part by text
 */


/** @namespace Parts.api/parts/ */
router.route('/')
    /** Get list of full parts 
   * @function
   * @memberof Parts.api/parts/
   * @name GET /api/parts/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @returns {PartFull[]} list of [PartFull]{@link module:Parts.PartFull }
   */
  .get(requireAuthACL(canUser = false, 'parts_list_ambit')
    ,controller.parts_parts)
  /** Get list of full parts with query
   * @function
   * @memberof Parts.api/parts/
   * @name GET /api/parts/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @param {QueryBody} QueryBody [QueryBody]{@link module:Parts.QueryBody }
   * @returns {PartFull[]} list of [PartFull]{@link module:Parts.PartFull}
   */
  .post(requireAuthACL()
    ,controller.parts_query)


/** @namespace Parts.api/parts/list/ */
router.route('/list')
  /** Get raw list of parts
   * @function
   * @memberof Parts.api/parts/list/
   * @name GET /api/parts/list/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @returns {Part[]} list of [Part]{@link module:Parts.Part}
   */
  .get(requireAuthACL(canUser = false, 'parts_ambit')
    ,controller.list_parts)

router.route('/names/list/')
  /** Get raw list of parts names
   * @function
   * @memberof Parts.api/parts/list/
   * @name GET /api/parts/names/list/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @returns {PartName[]} list of [PartName]{@link module:Parts.PartName}
   */
  .get(requireAuthACL()
    ,controller.list_names)

router.route('/groups/list/')
  /** Get raw list of groups
   * @function
   * @memberof Parts.api/parts/list/
   * @name GET /api/parts/groups/list/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @returns {PartGroup[]} list of [PartGroup]{@link module:Parts.PartGroup}
   */
  .get(requireAuthACL()
    ,controller.list_groups)

router.route('/makes/list/')
  /** Get raw list of makes
   * @function
   * @memberof Parts.api/parts/list/
   * @name GET /api/partsmakes/list/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @returns {PartCarMake[]} list of [PartCarMake]{@link module:Parts.PartCarMake}
   */
  .get(requireAuthACL()
    ,controller.list_makes)

router.route('/models/list/')
  /** GET /api/parts/models/list/ - Get list of models */
  /** Get raw list of models
   * @function
   * @memberof Parts.api/parts/list/
   * @name GET /api/parts/models/list/
   * @param {number=} offset=0 QUERY
   * @param {number=} limit=null QUERY
   * @returns {PartCarModel[]} list of [PartCarModel]{@link module:Parts.PartCarModel}
   */
  .get(requireAuthACL(canUser = true, 'parts_ambit')
    ,controller.list_models)


router.route('/:partId')
  /** Get full part by id 
   * @function
   * @memberof Parts.api/parts/
   * @name GET /api/parts/
   * @param {number} partId PARAM
   * @returns {PartFull} [PartFull]{@link module:Parts.PartFull}
   */
  .get(requireAuthACL()
    ,controller.get_part)

///** Load user when API with part id route parameter is hit */
router.param('partId', controller.load_part);

//router.route('/book/:partId')
  /** GET /api/parts/book - Book part 
  * @ignore*/
  //.get(controller.book)

module.exports = router;