const express = require('express');
const controller = require('./controller.js');


//var requireAuthACL 		 = require('./../../services/authentication').requireAuthACL;
//var createAccountLimiter = require('./../../services/limiter').createAccountLimiter;

const router = express.Router();


/** Express router providing user related routes
 * @module Users
 */

/** @namespace Users.api/users/ */
router.route('/miniapi')
	/** Get list of users
	 * @function
	 * @memberof Users.api/users/
	 * @name GET /api/users/
	 * @returns {User[]} list of [User]{@link module:Users.User}
	 */
	.get(controller.handleAresRequest)


module.exports = router;