const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
const dateFormat = require('dateformat');

/** @see Orders */
/**
 * Order type
 * @typedef {Object} module:Orders.Order
 * @property {string} Symbol unique AUTO
 * @property {number} NumberInMonth AUTO
 * @property {string} AdminsId ID of admin who did last modification
 * @property {char} DStatusesId FK to DStatuses
 * @property {date} Date cration date
 * @property {date} ModDate modification date
 * @property {char!} DShippingsId FK to DShippings
 * @property {string} ShippingMethod name of shipping method
 * @property {number} ShippingPrice price of shipping method
 * @property {char!} DPaymentsId FK to DPayments
 * @property {string=} UsersId FK to DShippings
 * @property {OrderElements[]} OrderElements
 * @property {OrderAddresses[]} OrderAddresses
 */
const Orders = database.define('Orders', {

  Symbol:{
    type: Sequelize.STRING(20) // nr/SKL/mm/yyyy
    ,allowNull: false
  }  

  ,NumberInMonth:{
    type: Sequelize.INTEGER
    ,allowNull: false
    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,AdminsId:{
    type: Sequelize.INTEGER
    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,DStatusesId:{
    type: Sequelize.STRING(1)
    ,allowNull: false
    ,defaultValue: 'N'
  }
 
  ,Date:{
    type: Sequelize.DATE
    ,defaultValue: Sequelize.NOW
    ,allowNull: false
    ,validate: {
      isDate: true
    }
  }  

  ,ModDate:{
    type: Sequelize.DATE
    ,defaultValue: null
    ,validate: {
      isDate: true
    }
  } 

  ,DShippingsId:{
    type: Sequelize.STRING(1)
  }

  ,ShippingMethod:{
    type: Sequelize.STRING
  }

  ,ShippingPrice:{
    type: Sequelize.DOUBLE
  }

  ,DPaymentsId:{
    type: Sequelize.STRING(1)
  }

  ,PaymentPrice:{
    type: Sequelize.DOUBLE
  }

  ,UsersId:{
    type: Sequelize.INTEGER
    ,validate: {
      isNumeric: true
      ,isInt: true
    }
  }

  ,IsInvoice:{
    type: Sequelize.BOOLEAN 
    ,defaultValue: false
  }

});

//static class methods

/**
 * Get part by id
 * @property {number} id - Parts id
 * @returns {part}
  */
Orders.get = async function(id){  
  var response = await Orders.findById(id)
    .then((orders) => {
      return orders; // eslint-disable-line no-param-reassign
    })
    .catch(err => {throw err})

    return response
  }

/**
 * Get part list within range.
 * @property {number} skip - Number of parts to be skipped.
 * @property {number} limit - Limit number of parts to be returned.
 * @returns {part[]}
  */
Orders.list = async function(range){
    range.order = Sequelize.col('Id')//adding order by id to findAll params
    return await this.findAll(range)
      .then(orders => {return orders})
      .catch(err => {throw err})
  }


Orders.getNextNumber = async function(){
  let qr = {};
  qr.order = [['NumberInMonth', 'DESC']];
  var d = '%' + dateFormat(new Date(), 'mm/yyyy')
  qr.where = {Symbol :{$like : d}};
  return await this.findOne(qr)
    .then(order => {
      if(!order)
        return 1;
      else
        return (order.NumberInMonth + 1);
    })
    .catch(err => {throw err})
}

Orders.getNextNumber = async function(){
  let qr = {};
  qr.order = [['NumberInMonth', 'DESC']];
  var d = '%' + dateFormat(new Date(), 'mm/yyyy')
  qr.where = {Symbol :{$like : d}};
  return await this.findOne(qr)
    .then(order => {
      if(!order)
        return 1;
      else
        return (order.NumberInMonth + 1);
    })
    .catch(err => {throw err})
}



//instance methods

//create or check if table exiest
//Part.sync();

module.exports = Orders