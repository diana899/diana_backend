var passportJWT = require("passport-jwt");

var ExtractJwt 	= passportJWT.ExtractJwt;


const expTime = 60;
const secretOrKey = process.env.TOKEN_KEY;

//'Bearer' + ' ' + token
var jwtOptions	= {}
jwtOptions.jwtFromRequest 	= ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey 		= secretOrKey;


module.exports = { expTime, secretOrKey, jwtOptions }