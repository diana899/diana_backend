const Sequelize = require('sequelize');
const database = require('../../../services/diana_db').database //tmp database!!
//tables //TODO pobierac modele z innego miejsca
const Orders = require('./../../orders/models/orders');

/** @see Dictionaries */

/**
 * Shipping type
 * @typedef {Object} module:Dictionaries.Shipping
 * @property {char} id Symbol of shipping (pk)
 * @property {string} Name Name of shipping
 * @property {string=} Comment Description of shipping
 * @property {number} Price Price of shipping method
 */
const DShippings = database.define('DShippings', {

  id:{
    type: Sequelize.STRING(1)
    ,primaryKey: true
    ,allowNull: false
    ,validate: {
      notEmpty: true
    }
  }

  ,Name:{
    type: Sequelize.STRING(32)
    ,validate: {
      notEmpty: true
    }
  }

  ,Comment:{
    type: Sequelize.STRING(256) 
  }

  ,Price:{
    type: Sequelize.DOUBLE

    ,validate: {
      isNumeric: true
    }
  }

  ,Icon:{
    type: Sequelize.STRING(256)
  }

});

//static class methods
DShippings.get = async function(id){  
  var response = await DShippings.findById(id)
    .then((dShipping) => {
      return dShipping;
    })
    .catch(err => {throw err})

    return response
  }

DShippings.list = async function(){
    return await this.findAll({order: Sequelize.col('Id')})
      .then(dShippings => {return dShippings})
      .catch(err => {throw err})
  }


//constraints
DShippings.hasMany(Orders, {foreignKey: 'DShippingsId', targetKey: 'id'})

//creating dictionaries (if not exiests)
DShippings.sync();



module.exports = DShippings