var express 	= require('express');

var requireAuthACL = require('./../services/authentication').requireAuthACL;
var requireAuth = require('./../services/authentication').requireAuth;
var getToken 		= require('./../services/authentication').getToken;

var router 		= express.Router();


// Test rout.
router.get('/', function(req, res, next) {
 	res.status(200).send({ message: 'No hejka! Co tam?'});
});

// Test get authentication.
router.get("/get/", function(req, res) {
    var token = getToken(UserId = 29);
    res.status(200).json({token: token});
});

// Test get authentication.
router.get("/get/admin/", function(req, res) {
	let admin = {
		id:1,
		AdminRole : {
				AdminRolePremissions: [
				{Symbol : 'ADMIN'}
			]
		}
	}
    var token = getToken({Admin : admin});
    res.status(200).json({token: token});
});


// Test authentication.
router.get("/auth/", requireAuthACL(), hello);

// Test authentication.
router.get("/auth/admin/", requireAuthACL('ADMIN'), hello);



function hello(req, res){
  res.json("Success! You can not see this without a token ");
}


module.exports = router;