const bcrypt = require('bcrypt');

const saltRounds = 10;

//callback hell : /
async function encrypt(plainPassword){
	return new Promise(function (resolve, reject){
		if(!plainPassword)
			reject(new Error('Hasło nie może być puste'))
		if(plainPassword.lenght <= 5)
			reject(new Error('Hasło musi mieć cnajmniej 5 znaków'))

		bcrypt.genSalt(saltRounds, (err, salt) => {
			bcrypt.hash(plainPassword, salt, (err, hash) => {
				if(!err)
					resolve(hash);
				else
					reject(err);
			});
		});
	});
}


async function decrypt(plainPassword, hash){
	return new Promise(function (resolve, reject){
		if(!plainPassword || !hash)
			reject(new Error('Hasło nie może być puste'))
		bcrypt.compare(plainPassword, hash).then(resp => {
			if(resp===true){
				resolve(true);		
			}
			else  
				resolve(false);
		})
		.catch(e => reject(e));
	});
}




module.exports = { encrypt, decrypt }