SET @id_role = (SELECT id FROM AdminRoles WHERE symbol = 'ADMIN');
DELETE FROM AdminRolePremissions WHERE AdminRolesId = @id_role;
Insert into AdminRolePremissions (AdminRolesId, Symbol, Comment)
Values
	(@id_role , 'user_list',		'GET users/')
	,(@id_role , 'create_user',		'POST users/')
	,(@id_role , 'get_user',		'GET users/:usersId')
	,(@id_role , 'update_user',		'PUT users/:usersId')
	,(@id_role , 'delete_user',		'DELETE users/:usersId')
    
	,(@id_role , 'orders_list',		'GET orders/')
	,(@id_role , 'orders_create',	'POST orders/')
    ,(@id_role , 'orders_get',		'GET orders/:orderId')
    ,(@id_role , 'orders_user_list','GET orders/user/:usersId')
    ,(@id_role , 'orders_status',	'PUT rdess/status/:orderId')
    ,(@id_role , 'user_list_anal',	'POST orders/list/anal/')
    
    ,(@id_role , 'parts_ambit',	'POST orders/list/anal/')
;